using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class TiroInimigo : MonoBehaviour
{
    [Header("Velocidade do tiro")]
    public float speed;

    [Header("Tempo de vida")]
    public float Timer;
    public float TempoDeVida;

    PlayerControll PlayerFather;
    Rigidbody2D rb;
    ArmaPlayer _ArmaPlayer;
    
    Vector2 direction;
    bool Refletiu;
    public SpriteRenderer SpriteR;


    private void Start()
    {                   
        rb = GetComponent<Rigidbody2D>();
        PlayerFather = FindObjectOfType<PlayerControll>();
        _ArmaPlayer = PlayerFather.EspadaObj.GetComponent<ArmaPlayer>();
    }

    private void OnEnable()
    {
        Refletiu = false;
        gameObject.tag = "TiroInimigo";
        if(SpriteR == null)
        {
            SpriteR = GetComponent<SpriteRenderer>();
        }
        SpriteR.flipY = false;
    }

    void Update()
    {
        if (!GameManager.Instance.Pause)
        {
            Timer += Time.deltaTime;

            if (!Refletiu)
            {
                rb.velocity = transform.up * speed;
            }
            else
            {
                rb.velocity = -direction * speed;
            }

            if (Timer > TempoDeVida)
            {
                reciclar();
            }
        }
        else
        {
            rb.velocity = Vector2.zero;
        }
    }

    void reciclar()
    {
        gameObject.SetActive(false);
        Timer = 0;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Reflect"))
        {
            if (_ArmaPlayer.CanReflect)
            {
                direction = collision.transform.position - transform.position;
                rb.velocity = Vector2.zero;
                Refletiu = true;
                gameObject.tag = "TiroPlayer";
                SpriteR.flipY = true;
            }
        }

        if (collision.gameObject.CompareTag("Parede"))
        {
            reciclar();
        }
    }
}
