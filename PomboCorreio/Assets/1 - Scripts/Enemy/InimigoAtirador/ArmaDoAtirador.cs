using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ArmaDoAtirador : MonoBehaviour
{
    [Header("Encontrar jogador")]
    public Transform Target;
    public float speed;
    public float rotationModifier;

    private void Start()
    {
        Target = FindObjectOfType<PlayerControll>().transform;
    }

    private void Update()
    {
            AlteraPosicaoArma();
    }

    void AlteraPosicaoArma()
    {
        Vector3 vectorToTarget = Target.transform.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg - rotationModifier;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * speed);
    }
}
