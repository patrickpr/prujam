using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using static UnityEngine.GraphicsBuffer;

public class InimigoAtirador : MonoBehaviour
{
    [Header("Spawnar Tiros")]
    public GameObject TiroInimigo;
    public Transform PosTiro;

    [Header("Reciclagem de Objetos")] 
    public int QuantidadeDeTiros;
    [HideInInspector] public List<GameObject> ObjetosInstanciados = new List<GameObject>();

    GameObject pool;

    EnemyInRoom InRoom;
    Animator Anim;
    BoxCollider2D BoxCollider;
    PlayerLife playerLife;
    SpriteRenderer SptRender;

    [Header("velocidade de tiros")]
    public float TempoParaAtirar;
    float Timer;
    bool Atirando;

    [Header("Vida")]
    public int HP;    
    [HideInInspector]public bool die;     
    public bool canTakeDmg;
    public float TimeToGetDmg;

    [SerializeField]
    private EventReference attackSound;

    [SerializeField]
    private EventReference swordImpact;

    [SerializeField]
    private EventReference arrowImpact;

    [SerializeField]
    private EventReference deathSound;

    private void Awake()
    {
        InRoom = GetComponent<EnemyInRoom>();
        Anim = GetComponent<Animator>();
        BoxCollider = GetComponent<BoxCollider2D>();
        playerLife = FindObjectOfType<PlayerLife>();
        SptRender = GetComponent<SpriteRenderer>();

        canTakeDmg = true;
    }

    IEnumerator DamageFlicker()
    {
        canTakeDmg = false;
        yield return new WaitForSeconds(TimeToGetDmg);
        canTakeDmg = true;
    }

    void TakeDamageSword()
    {
        if (canTakeDmg && HP > 0)
        {
            HP -= playerLife.GetDamage(gameObject.transform); 
            Anim.SetTrigger("Hit");
            StartCoroutine(DamageFlicker());
            CameraShake.instance.StartShake(0.1f, 0.02f);
            RuntimeManager.PlayOneShot(swordImpact);
        }
       
        if (HP <= 0)
        {
            BoxCollider.enabled = false;
            die = true;
            RuntimeManager.PlayOneShot(deathSound);
        }
        else
        {
            die = false;
        }
    }

    void Start()
    {
        pool = new GameObject("Tiros do Inimigo");
        pool.transform.parent = this.transform;
        for (int i = 0; i < QuantidadeDeTiros; i++)
        {
            GameObject obj = Instantiate(TiroInimigo, pool.transform);
            obj.SetActive(false);
            ObjetosInstanciados.Add(obj);
        }
    }

    public GameObject PegarTiroNaLista()
    {
        for (int i = 0; i < ObjetosInstanciados.Count; i++)
        {
            if (!ObjetosInstanciados[i].activeInHierarchy)
            {
                return ObjetosInstanciados[i];
            }
        }
        return null;
    }

    private void Update()
    {
        Anim.SetBool("CanAttack", InRoom.CanAttack);
        Anim.SetBool("Die", die);

        if (InRoom.CanAttack && !die && !Atirando)
        {
            Timer += Time.deltaTime;
            if(Timer>= TempoParaAtirar)
            {
                Anim.SetTrigger("Shoot");
                Timer = 0;
            }
        }

        if(transform.position.x > playerLife.transform.position.x)
        {
            SptRender.flipX = true;
        }
        else
        {
            SptRender.flipX = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("TiroPlayer"))
        {

            if (canTakeDmg && HP > 0)
            {
                HP -= collision.GetComponent<TiroPlayer>().GetDamage(gameObject.transform);
                Anim.SetTrigger("Hit");
                CameraShake.instance.StartShake(0.1f, 0.02f);
                RuntimeManager.PlayOneShot(arrowImpact);
            }

            if (HP <= 0)
            {
                BoxCollider.enabled = false;
                die = true;
                RuntimeManager.PlayOneShot(deathSound);
            }
            else
            {
                die = false;
            }
            
        }
        if (collision.gameObject.CompareTag("Attack"))
        {
            TakeDamageSword();
        }
    }

    public void Atirar()
    {
        if (!die)
        {            
            RuntimeManager.PlayOneShot(attackSound);
            GameObject Tiro = PegarTiroNaLista();

            if (Tiro != null)
            {
                Tiro.transform.position = PosTiro.position;
                Tiro.transform.rotation = PosTiro.rotation;
                Tiro.SetActive(true);
            }
            Timer = 0;
        }
    }

    public void soltagold() 
    {
        int quantidade = Random.Range(0, 6);
        GameManager.Instance.DropGold(quantidade, transform.position);
        Destroy(gameObject);
    }

    public void AtacandoTrue()
    {
        Atirando = true;
    }
    public void AtacandoFalse()
    {
        Atirando = false;
    }
}
