using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class EnemyFollow : MonoBehaviour
{

    [Header("Movimenta��o e HP")]    
    [SerializeField] float speed;
    [SerializeField] int HP;
    public bool canTakeDmg;
    public float TimeToGetDmg;
    //bool tocou;

    GameObject Target;
    Rigidbody2D Rb;
    EnemyInRoom InRoom;
    BoxCollider2D Box;
    public BoxCollider2D ColliderFisica;
    public bool nasceu;
    public bool die;
    PlayerLife playerLife;


    [Header("Movimentacao")]    
    float direcaoX;
    float direcaoY;
    Animator Anim;

    [Header("KnockBack")]
    [SerializeField] float KnockBackForce;
    [SerializeField] float KnockBackTime;    

    [Header("Cooldown")]
    [SerializeField] float CD_Timer;
    [SerializeField] float ExitCD_Timer;
    [HideInInspector]public bool Hit;

    [Header("Controle de distanciamento")]
    [SerializeField] public float RaioDoRaycast;
    [SerializeField] public float ForcaDeDistanciamento;
    Collider2D[] InimigosProximos;
    public LayerMask Layermask;

    [SerializeField]
    private EventReference swordImpact;

    [SerializeField]
    private EventReference arrowImpact;

    [SerializeField]
    private EventReference SomPassos;

    [SerializeField]
    private EventReference MorteSound;


    [SerializeField]
    private EventReference NascimentoEsqueleto;

    void Awake()
    {
        InRoom = GetComponent<EnemyInRoom>();
        Box = GetComponent<BoxCollider2D>();
        Rb = GetComponent<Rigidbody2D>();
        Hit = false;
        Target = FindObjectOfType<PlayerControll>().gameObject;
        Anim = GetComponent<Animator>();
        playerLife = FindObjectOfType<PlayerLife>();

        canTakeDmg = true;
    }

    private void Update()
    {
        if (!GameManager.Instance.Pause)
        {
            Anim.SetBool("CanAttack", InRoom.CanAttack);
            if (InRoom.CanAttack)
            {
                animations();
                if (nasceu && !die)
                {
                    CanFollow();
                    findPlayerReference();
                }
            }
        }

        Anim.SetBool("Die", die);
    }  

    void TakeDamage()
    {
        if (canTakeDmg && !die)
        {
            HP -= playerLife.GetDamage(gameObject.transform);
            Anim.SetTrigger("Hit");
            StartCoroutine(DamageFlicker());
            RuntimeManager.PlayOneShot(swordImpact);
            CameraShake.instance.StartShake(0.1f, 0.02f);
        }

        if (HP <= 0)
        {
            Box.enabled= false;
            ColliderFisica.enabled = false;
            Rb.velocity = Vector2.zero;
            die = true;
        }
        else
        {
            die = false;
        }
    }

    IEnumerator DamageFlicker()
    {
        canTakeDmg = false;
        yield return new WaitForSeconds(TimeToGetDmg);
        canTakeDmg = true;
    }



    void SegueTarget()
    {
        InimigosProximos = Physics2D.OverlapCircleAll(transform.position, RaioDoRaycast, Layermask);                                 //encontra os outros inimigos proximos de uma distancia(RaioDoRaycast)
        foreach (Collider2D ColiderDeInimigos in InimigosProximos)                                                                   //Pega os componentes collider2D de cada inimigo dentro do array
        {
            if (ColiderDeInimigos.gameObject.CompareTag("Enemy") && ColiderDeInimigos.gameObject != gameObject)                      //Compara com a tag Enemy e ve se esse objeto n�o � o proprio personagem
            {
                
                float Distancia = Vector2.Distance(transform.position, ColiderDeInimigos.transform.position);                        // Calcula a distancia do outro inimigo
                
                if (Distancia < RaioDoRaycast)                                                                                       //Se a distancia entre o inimigo e um outro inimigo for menos que o raio do raycast                
                {
                    
                    Vector2 DirecaoDoDistanciamento = (Vector2)transform.position - (Vector2)ColiderDeInimigos.transform.position;   // Calcula a dire�ao (pegando a posicao do inimigo e diminuindo do alvo proximo)

                    
                    transform.Translate(DirecaoDoDistanciamento.normalized * ForcaDeDistanciamento * Time.deltaTime, Space.World);   // Usa o Translate para se mover para longe
                }
            }
        }

        
        Vector2 direction = Target.transform.position - transform.position;                                                          // Determina a dire��o que vai seguir o jogador
        transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);                                             // Move o inimigo at� o jogador
    }

    void CanFollow()
    {
        if (!Hit)
        {
            SegueTarget();
        }

        if (Hit)
        {
            CD_Timer += Time.deltaTime;
            if (CD_Timer > ExitCD_Timer)
            {
                Hit = false;
                CD_Timer = 0;
            }
        }
    }

    void animations()
    {
        Anim.SetFloat("Horizontal", direcaoX);
        Anim.SetFloat("Vertical", direcaoY);
    }

    public void findPlayerReference()
    {
        direcaoX = Target.transform.position.x - transform.position.x;
        direcaoY = Target.transform.position.y - transform.position.y;
    }

    public void KnockBack(Vector2 direction)
    {
        Rb.velocity = Vector2.zero;
        Rb.AddForce(direction.normalized * KnockBackForce, ForceMode2D.Impulse);
        StartCoroutine(KnockBackTimer());
    }

    IEnumerator KnockBackTimer()
    {
        yield return new WaitForSeconds(KnockBackTime);
        Rb.velocity = Vector2.zero;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Attack"))
        {
            Hit = true;
            TakeDamage();
            Rb.velocity = Vector2.zero;            
            KnockBack((transform.position - collision.transform.position).normalized);                      
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            Hit = true;
            CameraShake.instance.StartShake(0.1f, 0.02f);
            Rb.velocity = Vector2.zero;
            KnockBack((transform.position - collision.transform.position).normalized);
        }

        if (collision.gameObject.CompareTag("TiroPlayer"))
        {
            Hit = true;
            Rb.velocity = Vector2.zero;

            if (canTakeDmg && !die)
            {
                HP -= collision.GetComponent<TiroPlayer>().GetDamage(gameObject.transform);
                Anim.SetTrigger("Hit");
                StartCoroutine(DamageFlicker());
                CameraShake.instance.StartShake(0.1f, 0.02f);
                RuntimeManager.PlayOneShot(arrowImpact);
            }

            if (HP <= 0)
            {
                Box.enabled = false;
                ColliderFisica.enabled = false;
                Rb.velocity = Vector2.zero;
                die = true;
            }
            else
            {
                die = false;
            }
        }
    }


    public void Nasceu()
    {
        if (!nasceu)
        {
            nasceu = true;
            Anim.SetBool("Nasceu", nasceu);           
        }
    }

    public void Morte()
    {
        soltagold();
        
        Destroy(gameObject);        
    }


    public void soltagold()
    {
        int quantidade = Random.Range(4, 8);
        GameManager.Instance.DropGold(quantidade, transform.position);
    }

    public void movimentoSound()
    {
        RuntimeManager.PlayOneShot(SomPassos);
    }

    public void BornSound()
    {
        RuntimeManager.PlayOneShot(NascimentoEsqueleto);
    }

    public void DeadSound()
    {
        RuntimeManager.PlayOneShot(MorteSound);
    }
}
