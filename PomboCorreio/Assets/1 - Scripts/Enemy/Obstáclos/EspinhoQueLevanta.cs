using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EspinhoQueLevanta : MonoBehaviour
{
    Animator Anim;

    public float Timer;
    public float TempoLevantar;

    public bool levantado;
    
    private void Awake()
    {
        Anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (!GameManager.Instance.Pause)
        {
            Timer += Time.deltaTime;

            if (Timer > TempoLevantar)
            {
                Timer = 0;
                levantado = !levantado;
            }
            Anim.SetBool("Levantado", levantado);

            if (levantado)
            {
                gameObject.layer = 0;
            }
            else
            {
                gameObject.layer = 9;
            }
        }
    }
}
