using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EsqueletoWalk : MonoBehaviour
{
    Animator Anim;
    GameObject Target;
    float direcaoX;
    float direcaoY;
    EnemyFollow enemy;
    EnemyInRoom InRoom;

    private void Start()
    {
        InRoom = GetComponent<EnemyInRoom>();
        Anim = GetComponent<Animator>();
        Target = FindObjectOfType<PlayerControll>().gameObject;
        enemy = GetComponent<EnemyFollow>();
    }

    private void Update()
    {
        if (!GameManager.Instance.Pause)
        {
            
            if (InRoom.CanAttack)
            {
                Animations();
                FindPlayerReference();
            }
        }
    }

    void Animations()
    {
        Anim.SetFloat("HorizontalIdle", direcaoX);
        Anim.SetFloat("VerticalIdle", direcaoY);
        Anim.SetBool("CD", enemy.Hit);
    }

    public void FindPlayerReference()
    {
        direcaoX = Target.transform.position.x - transform.position.x;
        direcaoY = Target.transform.position.y - transform.position.y;
    }
}
