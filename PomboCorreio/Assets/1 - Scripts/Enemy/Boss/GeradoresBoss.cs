using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class GeradoresBoss : MonoBehaviour
{
    public int HP;
    public bool canTakeDamage;
    public float TimeToGetDmg;

    PlayerLife playerLife;
    Animator Anim;

    [Header("Sons")]
    [SerializeField] private EventReference swordImpact;
    [SerializeField] private EventReference arrowImpact;

    private void Start()
    {
        playerLife = FindObjectOfType<PlayerLife>();
        Anim = GetComponent<Animator>();
        canTakeDamage = true;
    }

    private void OnEnable()
    {
        HP = 8;
        canTakeDamage = true;
    }

    void TakeDamage()
    {
        if (canTakeDamage && HP>0)
        {
            HP -= playerLife.GetDamage(gameObject.transform);
            Anim.SetTrigger("Hit");
            StartCoroutine(DamageFlicker());
        }

        if(HP <= 0)
        {
            gameObject.SetActive(false);
        }
    }

    IEnumerator DamageFlicker()
    {
        canTakeDamage = false;
        yield return new WaitForSeconds(TimeToGetDmg);
        canTakeDamage = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Attack"))
        {           
            TakeDamage();
            CameraShake.instance.StartShake(0.1f, 0.02f);
            RuntimeManager.PlayOneShot(swordImpact);
        }

        if (collision.gameObject.CompareTag("TiroPlayer"))
        {
            if (canTakeDamage && HP > 0)
            {
                HP -= collision.GetComponent<TiroPlayer>().GetDamage(gameObject.transform);
                Anim.SetTrigger("Hit");
                StartCoroutine(DamageFlicker());
                RuntimeManager.PlayOneShot(arrowImpact);
            }

            if (HP <= 0)
            {
                gameObject.SetActive(false);
            }
        }
    }

    IEnumerator VoltaVida()
    {
        yield return new WaitForSeconds(0.5f);
        HP = 3;
        canTakeDamage = true;
    }
}
