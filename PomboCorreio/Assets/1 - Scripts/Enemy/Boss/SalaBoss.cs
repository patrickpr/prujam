using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SalaBoss : MonoBehaviour
{
    public List<GameObject> InimigosNaSala = new List<GameObject>();
    public GameObject[] Portao;
    Boss Boss;
    PlayerLife Player;

    private void Start()
    {
        Boss = FindObjectOfType<Boss>();
        Player = FindObjectOfType<PlayerLife>();
    }

    private void Update()
    {
        if (!GameManager.Instance.Pause)
        {
            for (int i = 0; i < InimigosNaSala.Count; i++)
            {
                if (InimigosNaSala[i] == null)
                {
                    InimigosNaSala.Remove(InimigosNaSala[i]);
                }
            }
        }

        if (Boss.die || Player.die)
        {
            foreach (GameObject Inimigo in InimigosNaSala)
            {
                if (Inimigo != null)
                {
                   Inimigo.gameObject.SetActive(false);
                }
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            //Detecta o jogador na sala
            CameraController.instance.InBossRoom(true);

            //Detecta inimigos na sala
            foreach (GameObject Inimigo in InimigosNaSala)
            {
                if (Inimigo != null)
                {
                    if (!GameManager.Instance.Pause)
                    {
                        Inimigo.GetComponent<EnemyInRoom>().CanAttack = true;
                    }
                    else
                    {
                        Inimigo.GetComponent<EnemyInRoom>().CanAttack = false;
                    }
                }
            }

            //Detecta todos os port�es
            //Abre e fecha port�es caso o jogador esteja na sala do boss
            for (int i = 0; i < Portao.Length; i++)
            {
                if (Portao[i] != null)
                {
                    if (Boss.StartedFight)
                    {
                        Portao[i].GetComponent<Animator>().SetBool("HasEnemy", true);
                    }
                    else
                    {
                        Portao[i].GetComponent<Animator>().SetBool("HasEnemy", false);
                    }
                }
            }

        }

        //Adiciona os inimigos dentro do colider na lista de inimigos vivos na sala
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (!InimigosNaSala.Contains(collision.gameObject))
            {
                InimigosNaSala.Add(collision.gameObject);
            }
        }

    }

}