using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;

public class RaioBolaBoss : MonoBehaviour
{
    [Header("Velocidade do tiro")]
    public float speed;

    [Header("Tempo de vida")]
    public float Timer;
    public float TempoDeVida;

    PlayerControll PlayerFather;
    Rigidbody2D rb;
    ArmaPlayer _ArmaPlayer;

   public Vector2 Target;

    Vector2 direction;
    bool Refletiu;


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        PlayerFather = FindObjectOfType<PlayerControll>();
        _ArmaPlayer = PlayerFather.EspadaObj.GetComponent<ArmaPlayer>();
    }

    private void OnEnable()
    {
        Refletiu = false;
        gameObject.tag = "TiroInimigo";
        //Target = PlayerFather.gameObject.transform.position;
        
        Target = (PlayerFather.gameObject.transform.position - transform.position).normalized;
    }

    void Update()
    {
        if (!GameManager.Instance.Pause)
        {
            Timer += Time.deltaTime;

            if (!Refletiu)
            {
                //rb.velocity = ((PlayerFather.gameObject.transform.position - transform.position).normalized) * speed;
                rb.velocity = Target * speed;
            }
            else
            {
                rb.velocity = -direction * speed;
            }

            if (Timer > TempoDeVida)
            {
                Reciclar();
            }
        }
        else
        {
            rb.velocity = Vector2.zero;
        }
    }

    void Reciclar()
    {
        gameObject.SetActive(false);
        Timer = 0;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Reflect"))
        {
            if (_ArmaPlayer.CanReflect)
            {
                direction = collision.transform.position - transform.position;
                rb.velocity = Vector2.zero;
                Refletiu = true;
                gameObject.tag = "TiroPlayer";
            }
        }

        if (collision.gameObject.CompareTag("Parede"))
        {
            Reciclar();
        }
    }
}
