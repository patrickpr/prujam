using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootPoolingBoss : MonoBehaviour
{
    [Header("Spawnar Tiros")]
    public GameObject TiroPrefab;
    public Transform PosTiro;

    [Header("Reciclagem de Objetos")]
    public int QuantidadeDeTiros;
    public List<GameObject> ObjetosInstanciados = new List<GameObject>();

    GameObject pool;

    private void Start()
    {
        pool = new GameObject("Tiros do Boss");
        pool.transform.parent = this.transform;
        for (int i = 0; i < QuantidadeDeTiros; i++)
        {
            GameObject obj = Instantiate(TiroPrefab, pool.transform);
            obj.SetActive(false);
            ObjetosInstanciados.Add(obj);
        }
    }


    public GameObject PegarTiroNaLista()
    {
        for (int i = 0; i < ObjetosInstanciados.Count; i++)
        {
            if (!ObjetosInstanciados[i].activeInHierarchy)
            {
                return ObjetosInstanciados[i];
            }
        }
        return null;
    }


    public void Atirar()
    {
        GameObject Tiro = PegarTiroNaLista();

        if (Tiro != null)
        {
            Tiro.transform.position = PosTiro.position;
            Tiro.SetActive(true);
        }
    }
}
