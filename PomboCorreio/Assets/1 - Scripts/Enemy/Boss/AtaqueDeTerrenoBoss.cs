using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueDeTerrenoBoss : MonoBehaviour
{
    Animator anim; Boss Boss;

    public float timer;
    public float TimerToExplode;
    public float duracao;
    bool terminou;
    bool StartTimer;

    private void Start()
    {
        anim = GetComponent<Animator>(); Boss = FindObjectOfType<Boss>();
    }

    private void Update()
    {
        if(StartTimer)
        {
            if (!terminou)
            {

                timer += Time.deltaTime;
                if (timer > TimerToExplode)
                {
                    anim.SetTrigger("Explode");
                    terminou = true;
                }
            }
        }
    }

    public void Some()
    {
        Destroy(gameObject, duracao);
    }

    public void CanCount() 
    {
        StartTimer = true;
    }

    private void OnDestroy()
    {
        Boss.Especiais.Clear();
    }
}
