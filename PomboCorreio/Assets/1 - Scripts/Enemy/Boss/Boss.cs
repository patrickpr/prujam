using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Boss : MonoBehaviour
{
    [Header("Controlador de Vida")]
    [SerializeField] int HP;
    public bool die;
    [SerializeField] bool canTakeDmg;
    [SerializeField] float TimeToGetDmg;
    public Slider HpBar;

    public bool StartedFight;
    public GameObject[] Luzes;
    public int Estagios;

    Animator Anim;
    PlayerLife Player;
    BoxCollider2D Box;

    [Header("Instaciador de esqueletos")]
    [SerializeField] Transform[] LocalSpawn;    
    List<GameObject> Esqueletos = new List<GameObject>();
    [SerializeField] GameObject prefabEsqueleto;
    [SerializeField] int QuantEsqueleto;
    [SerializeField] float RespawnEsqueleto;

    public bool chamandoEsqueletos;
    bool TemEsqueletos;
    bool chamouEsqueletos; 
    GameObject pool;


    [Header("Barreira e Geradores")]
    [SerializeField] List<GameObject> GeradoresDeEscudo = new List<GameObject>();
    [SerializeField] GameObject Barreira;
    [SerializeField] float RespawnBarreira;

    public bool renasceu;
    public bool TemShield;

    [Header("Tiro Normal")]
    [SerializeField] float TimeToShoot;    
    ShootPoolingBoss PoolingShoots;
    float TimerTiroNormal;


    [Header("Pilar")]
    [SerializeField] GameObject Pilar;
    [SerializeField] GameObject Target;
    [SerializeField] int quantidadeRaios;
    [SerializeField] float TimeToPilar; 
    [SerializeField] float MaxCd_Pilar;
    float TimerCDPilar;
    bool CooldownPilar;
    bool chamouPilar;

    public bool ChamandoPilar;

    [Header("Especial")]
    [SerializeField] GameObject AtaqueEspecialObj;
    [SerializeField] float MaxCD_Especial;
    [SerializeField] public List<GameObject> Especiais = new List<GameObject>();
    bool cooldownEspecial;
    bool ChamouEspecial;
    public float TimerCD_Especial;

    public bool ChamandoEspecial;

    [Header("Sons")]
    [SerializeField] private EventReference swordImpact;
    [SerializeField] private EventReference arrowImpact;
    [SerializeField] private EventReference tower;
    [SerializeField] private EventReference area;
    [SerializeField] private EventReference ball;

    bool comecouluta;
    public GameObject[] ObjetosInvisiveisPreLuta;



    private void Awake()
    {
        PoolingShoots = FindObjectOfType<ShootPoolingBoss>();
        Anim = GetComponent<Animator>();
        Player = FindObjectOfType<PlayerLife>();
        Box = GetComponent<BoxCollider2D>();
    }

    private void Start()
    {
        cooldownEspecial = true;
        CooldownPilar = true; 
        HpBar.maxValue = HP;
    }

    void Update()
    {
        if (!GameManager.Instance.Pause && StartedFight && !die)
        {
            VerificaShield();
            RegeneraGeradores();
           
            if (TemShield)
            {
                if (!ChamandoPilar && !ChamandoEspecial)
                {
                    VerificaEsqueletos();
                    ChamaEsqueletos();
                }
                if (!chamandoEsqueletos && !ChamandoPilar && !ChamandoEspecial)
                {
                    NormalShoot();
                }
                if (Estagios > 1)
                {
                    if (!chamandoEsqueletos && !ChamandoPilar)
                    {
                        ChamaEspecial();
                    }

                    if (!ChamandoEspecial && !chamandoEsqueletos)
                    {
                        ChamaPilares();
                    }
                }
                
            }

            Animations();
        }

        if (StartedFight)
        {
            HpBar.gameObject.SetActive(true);
            ControllStages();
            Box.enabled = true;
            Anim.SetBool("Idle", false);
            if (!comecouluta)
            {
                foreach (GameObject Objetos in ObjetosInvisiveisPreLuta)
                {
                    Objetos.SetActive(true);
                }
                comecouluta = true;
            }
        }
        else
        {
            HpBar.gameObject.SetActive(false);
            Box.enabled = false;
            Anim.SetBool("Idle", true);
            foreach (GameObject Objetos in ObjetosInvisiveisPreLuta)
            {
                Objetos.SetActive(false);
            }
        }

        if (die)
        {
            StopAllCoroutines();
            foreach (GameObject Objetos in ObjetosInvisiveisPreLuta)
            {
                Objetos.SetActive(false);
            }
        }
    }

    void TakeDamage()
    {
        if (canTakeDmg && HP > 0)
        {
            HP -= Player.GetDamage(gameObject.transform);
            Anim.SetTrigger("Hit");
            StartCoroutine(DamageFlicker());
            RuntimeManager.PlayOneShot(swordImpact);
        }

        if (HP <= 0)
        {
            Box.enabled = false;
            die = true;
            Anim.SetBool("Die", die);
            
        }
        else
        {
            die = false;
        }
    }

    IEnumerator DamageFlicker()
    {
        canTakeDmg = false;
        yield return new WaitForSeconds(TimeToGetDmg);
        canTakeDmg = true;
    }


    bool VerificaShield()
    {        
        TemShield = false;
        foreach (GameObject Gerador in GeradoresDeEscudo)
        {
            if (Gerador.activeInHierarchy)
            {
                TemShield = true;
                break;
            }
        }
        return TemShield;
    }

    void RegeneraGeradores()
    {
        if (!TemShield)
        {
            if (!renasceu)
            {
                Barreira.SetActive(false);
                StartCoroutine(TimerReneradorGeradores());
                renasceu = true;
            }
        }
        else
        {
            Barreira.SetActive(true);
        }
    }

    bool VerificaEsqueletos()
    {
        TemEsqueletos = false;
        foreach (GameObject esqueleto in Esqueletos)
        {
            if (esqueleto != null)
            {
                TemEsqueletos = true;
                break;
            }
        }
        return TemEsqueletos;
    }

    void ChamaEsqueletos()
    {
        if (!TemEsqueletos)
        {
            if (!chamouEsqueletos)
            {
                List<GameObject> esqueletosNaoNulos = new List<GameObject>();

                foreach (GameObject esqueleto in Esqueletos)
                {
                    if (esqueleto != null)
                    {
                        esqueletosNaoNulos.Add(esqueleto);
                    }
                }

                Esqueletos = esqueletosNaoNulos;


                StartCoroutine(TimerToRespawnEsqueletos());
                chamouEsqueletos = true;
            }
        }
    }

    void CriaEsqueletos()
    {
        pool = new GameObject("Esqueletos");
        pool.transform.parent = this.transform;
        for (int i = 0; i < QuantEsqueleto; i++)
        {
            Vector3 pos = LocalSpawn[Random.Range(0, 3)].position;
            GameObject Esqueleto = Instantiate(prefabEsqueleto, pos, Quaternion.identity,pool.transform);
            Esqueletos.Add(Esqueleto);
        }       
    }

    void NormalShoot()
    {
        TimerTiroNormal += Time.deltaTime;
        if (TimerTiroNormal > TimeToShoot)
        {
            PoolingShoots.Atirar();
            TimerTiroNormal = 0;
            RuntimeManager.PlayOneShot(ball);
        }
    }

    void ChamaPilares()
    {
       
        if (!CooldownPilar)
        {
            if (!chamouPilar)
            {
                StartCoroutine(CriaPilares());
                chamouPilar = true;
            }
        }
        else
        {
            TimerCDPilar += Time.deltaTime;
            if(TimerCDPilar>= MaxCd_Pilar)
            {
                CooldownPilar = false;
                TimerCDPilar = 0;
            }

        }
    }

    void ChamaEspecial()
    {

        if (!cooldownEspecial)
        {
            if (!ChamouEspecial)
            {
                GameObject ataqueEspecial = Instantiate(AtaqueEspecialObj, Target.transform.position, quaternion.identity);
                Especiais.Add(ataqueEspecial);
                ChamouEspecial = true;
                cooldownEspecial = true;
            }
        }
        else
        {
            if (Especiais.Count <= 0)
            {
                TimerCD_Especial += Time.deltaTime;
                if (TimerCD_Especial >= MaxCD_Especial)
                {
                    StartCoroutine(AnimacaoEspecial());
                    cooldownEspecial = false;
                    ChamouEspecial = false;
                    TimerCD_Especial = 0;
                }
            }
        }
    }


    IEnumerator TimerToRespawnEsqueletos()
    {
        yield return new WaitForSeconds(RespawnEsqueleto);
        chamandoEsqueletos = true;
        //bota a anima��o aqui e bloqueia tudo por 1 segundo enquanto chama os esqueletos
        yield return new WaitForSeconds(1f);
        CriaEsqueletos();
        chamouEsqueletos = false;
        chamandoEsqueletos = false;
    }


    IEnumerator TimerReneradorGeradores()
    {

        yield return new WaitForSeconds(RespawnBarreira);
        foreach (GameObject Gerador in GeradoresDeEscudo)
        {
            Gerador.SetActive(true);
        }
        renasceu = false;

    }

    IEnumerator AnimacaoEspecial()
    {
        ChamandoEspecial = true;
        RuntimeManager.PlayOneShot(area);
        yield return new WaitForSeconds(0.5f);
        ChamandoEspecial = false;
    }

    IEnumerator CriaPilares()
    {
        ChamandoPilar = true;
        int i;

        if (!GameManager.Instance.Pause && StartedFight)
        {
            for (i = 0; i < quantidadeRaios; i++)
            {
                yield return new WaitForSeconds(TimeToPilar);
                Instantiate(Pilar, Target.transform.position, quaternion.identity);
                RuntimeManager.PlayOneShot(tower);
            }
            if (i >= quantidadeRaios)
            {
                CooldownPilar = true;
                chamouPilar = false;
                ChamandoPilar = false;
            }
        }
    }

    public void Animations()
    {
        Anim.SetBool("ChamandoEsqueletos", chamandoEsqueletos);
        Anim.SetBool("ChamandoEspecial", ChamandoEspecial);
        Anim.SetBool("ChamandoPilar", ChamandoPilar);
        Anim.SetBool("TemShield", TemShield);
    }

    public void ControllStages()
    {
        HpBar.value = HP;
        if(HP <= 50)
        {
            SoundManager.Instance.BossMusic(2);
            Estagios = 2;
        }
        if (die)
        {
            HpBar.gameObject.SetActive(false);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Attack"))
        {
            TakeDamage();
            CameraShake.instance.StartShake(0.1f, 0.02f);
        }

        if (collision.gameObject.CompareTag("TiroPlayer"))
        {

            CameraShake.instance.StartShake(0.1f, 0.02f);
            RuntimeManager.PlayOneShot(arrowImpact);

            if (canTakeDmg && HP > 0)
            {

                HP -= collision.GetComponent<TiroPlayer>().GetDamage(gameObject.transform);
                Anim.SetTrigger("Hit");
                StartCoroutine(DamageFlicker());
            }

            if (HP <= 0)
            {
                Box.enabled = false;
                die = true;
                Anim.SetBool("Die", die);
            }
            else
            {
                die = false;
            }


        }
    }

}