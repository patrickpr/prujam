using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PilarBoss : MonoBehaviour
{
    Animator anim;
    

    public float timer;
    public float TimerToExplode;
    bool terminou;

    private void Start()
    {
        anim = GetComponent<Animator>();
       
    }

    private void Update()
    {
        if (!terminou)
        {

            timer += Time.deltaTime;
            if (timer > TimerToExplode)
            {
                anim.SetTrigger("Explode");
                terminou = true;
            }
        }
    }

    public void Some()
    {
        Destroy(gameObject);
    }
}
