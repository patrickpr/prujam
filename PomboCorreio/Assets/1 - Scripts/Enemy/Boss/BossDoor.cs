using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BossDoor : MonoBehaviour
{
    public int QuantidadeDeChaves;

    Animator Anim;
    BoxCollider2D Box;
    public BoxCollider2D FilhoBox;

    public TextMeshProUGUI TextoPorta;

    [SerializeField]
    private EventReference openDoor;

    private void Start()
    {
        Anim = GetComponent<Animator>();
        Box = GetComponent<BoxCollider2D>();
        
        Box.enabled = true;
        FilhoBox.enabled = true;
        TextoPorta.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            QuantidadeDeChaves = ItemManager.Instance.GetNumberOfKeys();

            if (QuantidadeDeChaves >= 2)
            {
                Anim.SetTrigger("Abre");
                TextoPorta.enabled = false;
            }
            else
            {
                TextoPorta.enabled = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            QuantidadeDeChaves = ItemManager.Instance.GetNumberOfKeys();

            if (QuantidadeDeChaves < 2)
            {            
                TextoPorta.enabled = false;
            }
        }
    }


    public void OpenDoor()
    {
        Box.enabled = false;
        FilhoBox.enabled = false;
        RuntimeManager.PlayOneShot(openDoor);
    }


}
