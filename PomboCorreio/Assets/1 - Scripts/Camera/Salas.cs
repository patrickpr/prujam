using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salas : MonoBehaviour
{ 
    public List<GameObject> InimigosNaSala = new List<GameObject>();
    public GameObject[] Portao;

    private void Update()
    {
        if (!GameManager.Instance.Pause)
        {
            for (int i = 0; i < InimigosNaSala.Count; i++)
            {
                if (InimigosNaSala[i] == null)
                {
                    InimigosNaSala.Remove(InimigosNaSala[i]);
                }
            }
        }
       

    }
    
    private void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            //Move a camera para o centro da sala com suavidade
            CameraController.instance.SetPosition(this.transform.position, 3f * Time.deltaTime);

            //Detecta o jogador na sala
            CameraController.instance.InRoom(true);
            
            //Detecta inimigos na sala
            foreach (GameObject Inimigo in InimigosNaSala)
            {
                if (Inimigo != null)
                {
                    if (!GameManager.Instance.Pause)
                    {
                        Inimigo.GetComponent<EnemyInRoom>().CanAttack = true;                        
                    }
                    else
                    {
                        Inimigo.GetComponent<EnemyInRoom>().CanAttack = false;
                    }
                }
            }

            //Detecta todos os port�es
            //Abre e fecha port�es caso tenha inimigos na sala ou n�o
            for (int i = 0; i < Portao.Length; i++)
            {
                if (Portao[i] != null)
                {
                    if (InimigosNaSala.Count == 0)
                    {
                        Portao[i].GetComponent<Animator>().SetBool("HasEnemy", false);                       
                    }
                    else
                    {
                        Portao[i].GetComponent<Animator>().SetBool("HasEnemy", true);
                    }
                }
            }

           


        }        

        //Adiciona os inimigos dentro do colider na lista de inimigos vivos na sala
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (!InimigosNaSala.Contains(collision.gameObject))
            {
                InimigosNaSala.Add(collision.gameObject);
            }
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {            
            foreach (GameObject Inimigo in InimigosNaSala)
            {
                Inimigo.GetComponent<EnemyInRoom>().CanAttack = false;
            }
        }
    }

}
