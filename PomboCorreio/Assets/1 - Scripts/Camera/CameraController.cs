using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;
    public bool PlayerInRoom;
    public bool PlayerInBossRoom;
    public Transform Target;
    public float speedCam;
    public Vector2 offSet;

    FinalDoJogo Fim;
    GerenciadorGame Gerente;
    Boss Boss;
 
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(instance);
        }        

        Fim = FindObjectOfType<FinalDoJogo>();
        Gerente = FindObjectOfType<GerenciadorGame>();
    }

    private void Start()
    {
        GameManager.Instance.Pause = false;
        Boss = FindObjectOfType<Boss>();
    }

    public void SetPosition(Vector2 position,float tempo)
    {
        transform.position = new Vector3(Mathf.Lerp(transform.position.x, position.x,tempo), Mathf.Lerp(transform.position.y, position.y, tempo), 0);
    }

    public void InRoom(bool TaNaSala)
    {
        PlayerInRoom = TaNaSala; 
    }

    public void InBossRoom(bool SalaBoss)
    {
        PlayerInBossRoom = SalaBoss;
    }

    private void Update()
    {          
        if (!GameManager.Instance.Pause)
        {
            if (!PlayerInRoom || PlayerInBossRoom)
            {            
                Vector2 PosCam = transform.position;
                PosCam.y = Mathf.Lerp(transform.position.y, Target.position.y, speedCam * Time.deltaTime);
                PosCam.x = Mathf.Lerp(transform.position.x, Target.position.x, speedCam * Time.deltaTime);
                transform.position = PosCam;               
            }
        }
        if (transform.position.y < 103.2541)
        {
            if (Fim.MoveToBoss)
            {
                Vector2 PosCam = transform.position;
                PosCam.y = Mathf.Lerp(transform.position.y, Target.position.y + offSet.y, speedCam * Time.deltaTime);
                transform.position = PosCam;
            }
        }

        if (Boss.StartedFight)
        {
            this.GetComponent<Camera>().orthographicSize = 7;
        }
        if (Gerente.DerrotaScreen || Gerente.VitoriaScreen)
        {
            this.GetComponent<Camera>().orthographicSize = 5.8f;
        }
    }
}
