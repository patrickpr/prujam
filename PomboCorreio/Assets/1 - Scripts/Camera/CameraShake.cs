using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public static CameraShake instance;

    float shakeTimeRemaining, shakePower, shakeFadeTime, shakeRotation;
    public float rotationMultiplier;

     bool getPosition;
     Vector3 PosicaoCam;
    
    private void Awake()
    {      
        if (instance != null)
        {
            Destroy(gameObject);
        }
        if (instance == null)
        {
            instance = this;
        }
    }

    // Serve para chamar uma fun��o ap� o update, realizando as duas, por�m essa � realizade em um momento antes do final do frame
    private void LateUpdate()
    {
        if (!GameManager.Instance.Pause)
        {
            if (!getPosition)
            {
                PosicaoCam = transform.position;
                getPosition = true;
            }

            if (shakeTimeRemaining > 0) //Aqui � para ver e o tempo de treme��o ainda � maior que 0, a variavel que vai dar inicio ao codigo
            {
                shakeTimeRemaining -= Time.deltaTime; //Enquanto ocorrem as coisas dentro do if, essa linha vai diminuir o tempo, fazendo ela acabar
                float xAmout = Random.Range(-1f, 1) * shakePower;// aqui o valor de X recebe um numero aleatorio entre -1 e 1
                float yAmout = Random.Range(-1f, 1) * shakePower;// aqui o valor de Y recebe um numero aleatorio entre -1 e 1

                transform.position += new Vector3(xAmout, yAmout, 0f); //Aqui a posi��o da camera vai receber o valores dados no X e Y amount, citados acima, que vai fazer a camera efetivamente mexer
                shakePower = Mathf.MoveTowards(shakePower, 0f, shakeFadeTime * Time.deltaTime); //aqui os valores de potencia da treme��o vao ir do valor dado, no caso do input la em cima � .3f, at� 0 e no prazo do fadeTime, para ser mais suave
                shakeRotation = Mathf.MoveTowards(shakeRotation, 0f, shakeFadeTime * rotationMultiplier * Time.deltaTime);//Codigo Similar ao de cima, por�m � feito com o rotation e multiplica pelo rotation multiplier para manter mais suave ainda
            }
            else
            {
                shakeTimeRemaining = 0f;
                this.transform.position = new Vector3(PosicaoCam.x, PosicaoCam.y, PosicaoCam.z);
                getPosition = false;
            }
            transform.rotation = Quaternion.Euler(0f, 0f, shakeRotation * Random.Range(-1f, 1f)); // aqui � o momento em que eu fa�o a camera efetivamente girar para a posi��o do shakerotation que logo acima tamb�m est� diminuindo
        }
    }

    public void StartShake(float duracao, float potencia)//Um metodo publico para ser chamado por outros scripts usando sobrecarga
    {
        
        shakeTimeRemaining = duracao; // o valor dado no momento em que o metodo � chamado vai ser passado para o tempo de dura��o deste script, fazendo tudo funcionar como deve
        shakePower = potencia;// o valor dado no momento em que o metodo � chamado vai ser passado para a potencia deste script, fazendo tudo funcionar como deve

        shakeFadeTime = potencia / duracao; //uma divisao entre poder e dura��o para encontrar um numero pequeno para o fade
        shakeRotation = potencia * rotationMultiplier;// uma multiplica��o entre o valor de potencia com o multiplicador de poder para ter uma suavidade no momento da tremida rotacional
    }

}
