using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp
{
    public int id;
    public string description;
    public bool isActivated;

    public PowerUp(int id, string description)
    {
        this.id = id;
        this.description = description;
    }

    public void ActivatePowerUp()
    {
        this.isActivated = true;
    }
}
