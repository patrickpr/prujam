using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class ControleLoja : MonoBehaviour
{
    public GameObject CaixaDialogo;

    public int valorAtual;
    public int BotaoID;

    public GameObject TelaConfirmacao;
    public Botoes[] ListaDeBotoes;

    public TextMeshProUGUI UI_Gold;
    public TextMeshProUGUI UI_Cartas;

    [SerializeField]
    private EventReference clickSound;

    [SerializeField]
    private EventReference buyItemSound;


    private void Start()
    {
        RefreshButtons();
        UI_Cartas.text = ItemManager.Instance.GetNumberOfLetters().ToString() + "/5";

        if (GameManager.Instance.ContadorDeMortes == 1)
        {
            CaixaDialogo.SetActive(true);
        }
    }

    private void Update()
    {
        for (int i = 0; i < ListaDeBotoes.Length; i++)
        {
            if (i == BotaoID)
            {
                if (ListaDeBotoes[i] != null)
                {
                    valorAtual = ListaDeBotoes[i].Valor;
                }
            }
        }
        UI_Gold.text = GameManager.Instance.Gold.ToString();
    }


    public void OnClick()
    {
        if(GameManager.Instance.Gold>= valorAtual)
        {
            RuntimeManager.PlayOneShot(clickSound);
            TelaConfirmacao.SetActive(true);
        }
    }

    public bool GetActiveButtonState()
    {
        if (ListaDeBotoes[BotaoID] && ListaDeBotoes[BotaoID].isActiveAndEnabled)
        {
            return true;
        }
        return false;
    }

    public void Confirma()
    {
        RuntimeManager.PlayOneShot(buyItemSound);
        GameManager.Instance.Gold -= valorAtual;
        
        TelaConfirmacao.SetActive(false);

        PowerUpManager.Instance.GetPowerUpById(BotaoID).ActivatePowerUp();
        ListaDeBotoes[BotaoID].GetComponent<Button>().interactable = false;

    }

    public void Cancela()
    {
        TelaConfirmacao.SetActive(false);
    }


    private void RefreshButtons()
    {
        if (PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.HP).isActivated)
        {
            ListaDeBotoes[(int)PowerUpManager.IDs.HP].GetComponent<Button>().interactable = false;
        }
        if (PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.Dash).isActivated)
        {
            ListaDeBotoes[(int)PowerUpManager.IDs.Dash].GetComponent<Button>().interactable = false;
        }
        if (PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.Bow).isActivated)
        {
            ListaDeBotoes[(int)PowerUpManager.IDs.Bow].GetComponent<Button>().interactable = false;
        }
        if (PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.Reflect).isActivated)
        {
            ListaDeBotoes[(int)PowerUpManager.IDs.Reflect].GetComponent<Button>().interactable = false;
        }
        if (PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.Damage).isActivated)
        {
            ListaDeBotoes[(int)PowerUpManager.IDs.Damage].GetComponent<Button>().interactable = false;
        }
        if (PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.Critical).isActivated)
        {
            ListaDeBotoes[(int)PowerUpManager.IDs.Critical].GetComponent<Button>().interactable = false;
        }
    }
}
