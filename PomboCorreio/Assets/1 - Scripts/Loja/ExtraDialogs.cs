using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class ExtraDialogs : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI extraText;
    public string[] FalasExtras;

    void Start()
    {
        if(GameManager.Instance.ContadorDeMortes > 1){
            extraText.text = GetRandomDialog();
        }
        else
        {
            extraText.text = string.Empty;
            gameObject.SetActive(false);
        }
    }

    private string GetRandomDialog()
    {
        var index = Random.Range(0, FalasExtras.Length - 1);
        return FalasExtras[index];
    }
}
