using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using FMODUnity;

public class Botoes : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    ControleLoja Loja;

    public int Valor;

    [SerializeField]
    private PowerUpManager.IDs PowerUp;

    public TextMeshProUGUI texto;
    public TextMeshProUGUI Titulo;
    public TextMeshProUGUI DescricaoTxt;

    public string Descricao;

    public string TituloInicial;
    public string DescricaoInicial;

    [SerializeField]
    private EventReference hoverSound;

    private void Awake()
    {
        Loja = FindObjectOfType<ControleLoja>();
        texto = GetComponentInChildren<TextMeshProUGUI>();

        texto.text = Valor.ToString();
        TituloInicial = Titulo.text;
        DescricaoInicial = DescricaoTxt.text;
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        Loja.BotaoID = (int)PowerUp; 
        Titulo.text = gameObject.name;
        DescricaoTxt.text = Descricao;

        if (Loja.GetActiveButtonState())
        {
            RuntimeManager.PlayOneShot(hoverSound);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Titulo.text = TituloInicial;
        DescricaoTxt.text = DescricaoInicial;
    }
}
