using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Inventario : MonoBehaviour
{
    [SerializeField]
    List<ButtonInventario> buttons = new List<ButtonInventario>();

    public TextMeshProUGUI TextDescription;

    void OnEnable()
    {
        foreach (ButtonInventario button in buttons)
        {
            if (ItemManager.Instance.GetItemById((int)button.ID) != null)
            {
                button.gameObject.SetActive(true);
            }
            else
            {
                button.gameObject.SetActive(false);
            }
        }
    }
}
