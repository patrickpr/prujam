using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ButtonInventario : MonoBehaviour
{
    [SerializeField]
    public ItemManager.ItemIDs ID;
    public TextMeshProUGUI TextNome;

    Inventario Mochila;

    private void Start()
    {
        Mochila = FindObjectOfType<Inventario>();
    }

    private void OnEnable()
    {
        TextNome = GetComponentInChildren<TextMeshProUGUI>();
        if (ItemManager.Instance != null && ItemManager.Instance.GetItemById((int)ID) != null)
        {
            TextNome.text = ItemManager.Instance.GetItemById((int)ID).GetName();
        }
    }

    public void OnClick()
    {
        Mochila.TextDescription.text = ItemManager.Instance.GetItemById((int)ID).GetDescription();
    }


}
