using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Armazenar dados referentes ao itens do inventario e coletaveis
public class Item
{
    private ItemManager.ItemTypes type;
    private int id;
    private string name;
    private string description;
    private Sprite sprite;


    public Item(ItemManager.ItemTypes type, int id, string name, string description, Sprite sprite)
    {
        this.type = type;
        this.id = id;
        this.name = name;
        this.description = description;
        this.sprite = sprite;
    }

    public int GetId()
    {
        return id;
    }

    public ItemManager.ItemTypes GetItemType()
    {
        return type;
    }

    public string GetDescription()
    {
        return description;
    }

    public string GetName()
    {
        return name;
    }

}
