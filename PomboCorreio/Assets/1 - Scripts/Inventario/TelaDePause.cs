using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TelaDePause : MonoBehaviour
{
    public GameObject Mochila;
    public GameObject Options;

    public Sprite[] Imagens;
    public GameObject AbaMochilaOBJ;
    public GameObject AbaOptionsOBJ;
    public GameObject[] Icones;

    public TextMeshProUGUI Cartas;
    public TextMeshProUGUI Chaves;
    public TextMeshProUGUI Moedas;



    private void OnEnable()
    {
        AbaMochila();
        foreach (GameObject icon in Icones)
        {
            icon.SetActive(true);
        }

        Cartas.text = ItemManager.Instance.GetNumberOfLetters().ToString() + "/5";
        Chaves.text = ItemManager.Instance.GetNumberOfKeys().ToString() + "/2";
        Moedas.text = GameManager.Instance.Gold.ToString();
    }

    private void OnDisable()
    {
        foreach (GameObject icon in Icones)
        {
            icon.SetActive(false);
        }
    }

    public void AbaMochila()
    {
        Mochila.SetActive(true);
        AbaMochilaOBJ.GetComponent<Image>().sprite = Imagens[0];
        AbaOptionsOBJ.GetComponent<Image>().sprite = Imagens[1];
        Options.SetActive(false);
    }

    public void AbaOptions()
    {
        Mochila.SetActive(false);
        AbaMochilaOBJ.GetComponent<Image>().sprite = Imagens[1];
        AbaOptionsOBJ.GetComponent<Image>().sprite = Imagens[0];
        Options.SetActive(true);
    }
}
