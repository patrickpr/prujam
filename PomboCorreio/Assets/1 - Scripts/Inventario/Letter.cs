using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Letter : MonoBehaviour
{
    private void Start()
    {
        ItemGameObject itemGameObject = GetComponent<ItemGameObject>();
        
        if (ItemManager.Instance.GetItemById(itemGameObject.GetId()) != null && (itemGameObject.GetItemType() == ItemManager.ItemTypes.Letter || itemGameObject.GetItemType() == ItemManager.ItemTypes.Key))
        {
            if (!gameObject.IsDestroyed())
            {
                Destroy(gameObject);
            }
        }
    }

}
