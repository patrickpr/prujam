using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

//Relaciona o item com o gameobject da unity
public class ItemGameObject : MonoBehaviour
{
    [SerializeField]
    private ItemManager.ItemTypes type;

    [SerializeField]
    private ItemManager.ItemIDs id;

    [SerializeField]
    private string itemName;

    [SerializeField]
    private string description;

    [SerializeField]
    private Sprite sprite;

    private UIController controller;

    [SerializeField]
    private EventReference itemSound;

    [SerializeField]CartasHUD CartasHUD;

    private void Awake()
    {
        controller = FindObjectOfType<UIController>();
    }

    public int GetId()
    {
        return (int)id;
    }

    public ItemManager.ItemTypes GetItemType()
    {
        return type;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            RuntimeManager.PlayOneShot(itemSound);
            ItemManager.Instance.AddItem(new Item(type, (int)id, itemName, description,sprite));
            if (type == ItemManager.ItemTypes.Letter)
            {
                controller.UpdateLetters();     //Atualiza o texto da HUD das cartas
                CartasHUD.gameObject.SetActive(true);
            }
            Destroy(gameObject);
        }
    }
}
