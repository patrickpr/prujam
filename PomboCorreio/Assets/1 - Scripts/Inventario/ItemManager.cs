using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public static ItemManager Instance;
    private List<Item> items = new List<Item>();

    public enum ItemTypes
    {
        Letter = 0,
        Consumable = 1,
        Key =2
    }

    public enum ItemIDs
    {
        Letter1 = 1,
        Letter2 = 2,
        Letter3 = 3,
        Letter4 = 4,
        Letter5 = 5,
        Letter6 = 6,
        Key1 = 7,
        Key2 = 8,
    }

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public List<Item> GetItems()
    {
        return items;
    }

    public void AddItem(Item item)
    {
        items.Add(item);
    }

    public Item GetItemById(int id)
    {
        return items.Find(x => x.GetId() == id);
    }

    public int GetNumberOfLetters()
    {
        return GetItems().FindAll(x => x.GetItemType() == ItemManager.ItemTypes.Letter).Count;
    }

    public int GetNumberOfKeys()
    {
        return GetItems().FindAll(x => x.GetItemType() == ItemManager.ItemTypes.Key).Count;
    }

    public void RemoveKeys()
    {
        //foreach (Item item in items.ToList())
        //{
        //    if(item.GetItemType() == ItemTypes.Key)
        //    {
        //        items.Remove(item);
        //    }
        //}
        items.RemoveAll(x =>x.GetItemType() == ItemTypes.Key);
    }
}
