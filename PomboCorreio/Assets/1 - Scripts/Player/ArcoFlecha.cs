using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ArcoFlecha : MonoBehaviour
{
    PlayerControll Player;

    Vector3 MousePos;
    [SerializeField] float offset;
    [SerializeField] Transform PlayerTransform;
    ObjectPoolingPlayer Pooling;
    [SerializeField] EventReference windBowSound;


    private void Start()
    {
        Pooling = FindObjectOfType<ObjectPoolingPlayer>();
        Player = FindObjectOfType<PlayerControll>();
    }

    private void Update()
    {
        PosicaoArco();
    }
    void PosicaoArco()
    {

        MousePos = Mouse.current.position.ReadValue();                                              //Le valor do mouse
        var dir = MousePos - Camera.main.WorldToScreenPoint(transform.position);                    //mostra a dire��o do mouse baseado no localdele na camera    
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.eulerAngles = new Vector3(0, 0, angle);


        Vector3 playerToMouseDir = Camera.main.ScreenToWorldPoint(MousePos) - PlayerTransform.position;
        playerToMouseDir.z = 0;
        transform.position = PlayerTransform.position + (offset * playerToMouseDir.normalized);

    }

    public void Tiro()
    {
        RuntimeManager.PlayOneShot(windBowSound);
        Pooling.Atirar();
    }

    public void InicioAtaque()
    {
        Player.Atacando = true;
        Player.ContadorAtacando = 0.1f;
    }

}
