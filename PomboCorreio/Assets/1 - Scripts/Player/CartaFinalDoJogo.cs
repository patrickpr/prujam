using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartaFinalDoJogo : MonoBehaviour
{
    Animator Anim;

    GerenciadorGame Gerente;

    private void Awake()
    {
        Anim = GetComponent<Animator>();
        Gerente = FindObjectOfType<GerenciadorGame>();
    }
    private void OnEnable()
    {
        Anim.SetTrigger("Chamou");
    }


    public void ChamaFinalDoJogo()
    {
        Gerente.StartDialog=true;
    }
        
}
