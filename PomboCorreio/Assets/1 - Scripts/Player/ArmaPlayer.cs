using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ArmaPlayer : MonoBehaviour
{
    PlayerControll Player;
    Vector3 MousePos;
    [SerializeField] float offset;
    [SerializeField] Transform PlayerTransform;
    Animator Anim;
    BoxCollider2D Box;
    public bool CanReflect;
    [SerializeField] EventReference swordAttackSound;
    private bool haveReflect;

    private void Start()
    {
        Anim = GetComponent<Animator>();
        Box = GetComponent<BoxCollider2D>();
        Player = FindObjectOfType<PlayerControll>();

        if (PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.Reflect).isActivated)
        {
            haveReflect = true;
        }
    }

    private void Update()
    {
        PosicaoEspada();

        Anim.SetFloat("Horizontal", Player.dir.x - Player.transform.position.x);
        Anim.SetFloat("Vertical", Player.dir.y - Player.transform.position.y);
    }

    void PosicaoEspada()
    {
        MousePos = Mouse.current.position.ReadValue();                                              //Le valor do mouse
        var dir = MousePos - Camera.main.WorldToScreenPoint(transform.position);                    //mostra a dire��o do mouse baseado no local dele na camera    

        Vector3 playerToMouseDir = Camera.main.ScreenToWorldPoint(MousePos) - PlayerTransform.position;      
        playerToMouseDir.z = 0;
        transform.position = PlayerTransform.position + (offset * playerToMouseDir.normalized);

        Vector3 localScale = transform.localScale;        
        transform.localScale = localScale;
    }

    void AtiveBox()
    {
        RuntimeManager.PlayOneShot(swordAttackSound);
        Box.enabled= true;
        if (haveReflect)
        {
            CanReflect = true;
        }
        Player.Atacando = true;
        Player.ContadorAtacando = 0.32f;
    }
    void DesativeBox()
    {
        Box.enabled = false;
        CanReflect = false;
    }
    public void desativar()
    {
        this.gameObject.SetActive(false);
    }

}
