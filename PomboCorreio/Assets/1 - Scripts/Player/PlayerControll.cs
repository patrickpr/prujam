using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerControll : MonoBehaviour
{
    Rigidbody2D Rb;
    Animator Anim;
    [HideInInspector]public Vector2 Movimento;
    PlayerInputs PlayerInput;
    Vector3 MousePos;
    [HideInInspector] public Vector3 dir;

    
    [Header("Valores")]
    [SerializeField] float Speed;

    [Header("Dash Controll")]
    public bool UsandoDash;
    [SerializeField] float DashForce;
    [SerializeField] float TempoDeDash;
    public float CdDash;
    bool ClickToDash;
    float DuracaoDash;
    public float TimerCdDash;

    [Header("Arco/Espada")]
    public GameObject ArcoObj;
    public GameObject EspadaObj;
    public float ContadorTiroMax;
    
    /*[HideInInspector]*/public bool Atacando;
    
    bool UsandoArco;
    bool UsandoEspada;
    bool EsperandoCdTiro;   
    float ContadorTiro;

    [SerializeField] private EventReference dashSound;
    [SerializeField] private EventReference passinhosSound;

    [SerializeField]
    private bool haveBow;
    private bool fastDash;

    [Header("TemposDosTemporarios")]
    float InicialSpeed;
    public bool  SpeedUP;
    public float TimerSpeed;

    public float ContadorAtacando;


    private void Awake()
    {
        Rb = GetComponent<Rigidbody2D>();
        Anim = GetComponent<Animator>();
        PlayerInput = new PlayerInputs();
    }

    private void Start()
    {
        UsandoEspada = true;
        UsandoArco = false;
        ArcoObj.SetActive(false);

        haveBow = PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.Bow).isActivated;
        fastDash = PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.Dash).isActivated;
        InicialSpeed = Speed;
    }
    void OnEnable()
    {
        PlayerInput.Enable();
    }
    void OnDisable()
    {
        PlayerInput.Disable();
    }


    private void FixedUpdate()
    {
        if (!GameManager.Instance.Pause)
        {
            if (!UsandoDash)
            {
                Rb.velocity = Movimento * Speed;
            }
        }
        else
        {
            Rb.velocity = Vector2.zero;
            Movimento = Vector2.zero;
        }
    }

    private void Update()
    {
        Animacoes();
        if (!GameManager.Instance.Pause)
        {           
            if (ClickToDash)
            {
                DashPlayer();
                RuntimeManager.PlayOneShot(dashSound);
            }
            if (!UsandoDash && TimerCdDash > 0)
            {
                TimerCdDash -= Time.deltaTime;
            }

            if (EsperandoCdTiro)
            {
                ContadorTiro += Time.deltaTime;
                if (ContadorTiro >= ContadorTiroMax)
                {
                    ContadorTiro = 0;
                    EsperandoCdTiro = false;
                }
            }
        }

        if (Atacando)
        {            
            if (ContadorAtacando>0)
            {
                ContadorAtacando -= Time.deltaTime;
            }
            else
            {
                Atacando = false;
            }
        }
        

        if (UsandoDash)
        {
            gameObject.layer = 8; 
        }
        else
        {
            gameObject.layer = 0;
        }

        if (SpeedUP)
        {
            TimerSpeed -= Time.deltaTime;
            if (TimerSpeed <= 0)
            {
                SpeedUP = false;
                Speed = InicialSpeed;
                TimerSpeed = 0;
            }
            else
            {
                Speed = 9;
            }
        }
    }

    public void Movimento4D(InputAction.CallbackContext value)
    {
        if (!GameManager.Instance.Pause)
        {
            Movimento = value.ReadValue<Vector2>();
        }
    }
    public void Attack(InputAction.CallbackContext context) 
    {
        if (!GameManager.Instance.Pause)
        {
            if (context.performed)
            {
                AtaquePlayer();
            }
            else
            {
                return;
            }
        }
    }
    public void TrocarArma(InputAction.CallbackContext context)
    {
        if (!GameManager.Instance.Pause)
        {
            if (context.performed)
            {
                if (haveBow)
                {
                    if (UsandoEspada)
                    {
                        UsandoEspada = false;
                        UsandoArco = true;

                        EspadaObj.SetActive(false);
                        ArcoObj.SetActive(true);
                    }
                    else
                    {
                        UsandoArco = false;
                        UsandoEspada = true;

                        ArcoObj.SetActive(false);
                    }
                }
            }
        }
    }
    public void Dash(InputAction.CallbackContext context)
    {
        if (!GameManager.Instance.Pause)
        {
            if (context.performed)
            {
                if (TimerCdDash <= 0 && !UsandoDash)
                {
                    ClickToDash = true;                                                             //Variavel que serve para eu saber se cliquei para dar o dash
                    UsandoDash = true;                                                              //Serve para saber que est� acontecendo o dash
                    DuracaoDash = TempoDeDash;                                                      //Faz a dura��o do Dash receber o tempo que eu determinar pra ser
                    Vector2 direcaoDash = Movimento;//(dir - transform.position).normalized;                    //Localiza a posi��o do mouse e transforma em numero entre -1 e 1
                    Rb.velocity = DashForce * direcaoDash;                                          //Acrescenta velocidade ao jogador baseada na for�a dada e multiplicando pela dire��o para ir ao lugar certo
                }
            }
        }
    }   
    
    void DashPlayer()
    {
        if (DuracaoDash <= 0)
        {
            UsandoDash = false;
            DuracaoDash = 0;
            ClickToDash = false;

            if (fastDash)
            {
                TimerCdDash = CdDash/2.6f;
            }
            else
            {
                TimerCdDash = CdDash;
            }
        }
        else
        {
            DuracaoDash -= Time.deltaTime;
        }
       
    }
    
    void AtaquePlayer()
    {
        if (!GameManager.Instance.Pause)
        {
            if (!Atacando)
            {
                if (UsandoArco)
                {
                    if (!EsperandoCdTiro)
                    {
                        ArcoObj.GetComponent<Animator>().SetTrigger("Shoot");
                        EsperandoCdTiro = true;
                    }
                }
                if (UsandoEspada)
                {
                    EspadaObj.SetActive(true);
                }
            }
        }
    }
    
    void Animacoes()
    {
        MousePos = Mouse.current.position.ReadValue();
        dir = Camera.main.ScreenToWorldPoint(MousePos);

        if (Atacando)
        {
            if (Movimento == Vector2.zero)
            {
                Anim.SetFloat("HorizontalIdle", dir.x - transform.position.x);
                Anim.SetFloat("VerticalIdle", dir.y - transform.position.y);
            }
            else
            {
                Anim.SetFloat("Horizontal",  dir.x - transform.position.x);
                Anim.SetFloat("Vertical",    dir.y - transform.position.y);
            }

            
        }

        if (!Atacando)
        {
            Anim.SetFloat("Horizontal", Movimento.x);
            Anim.SetFloat("Vertical", Movimento.y);
        }
        Anim.SetFloat("speed", Movimento.sqrMagnitude);
        Anim.SetBool("UsandoDash", UsandoDash);
        
        //if (Movimento == Vector2.zero)
        //{
        //    Anim.SetFloat("HorizontalIdle", dir.x - transform.position.x);
        //    Anim.SetFloat("VerticalIdle", dir.y - transform.position.y);
        //}

        if (Movimento != Vector2.zero)
        {
            Anim.SetFloat("HorizontalIdle", Movimento.x);
            Anim.SetFloat("VerticalIdle", Movimento.y);
        }
    }


    public void SomPassos()
    {
        RuntimeManager.PlayOneShot(passinhosSound);
    }

   
}
