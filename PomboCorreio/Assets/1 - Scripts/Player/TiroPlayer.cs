using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class TiroPlayer : MonoBehaviour
{
    [Header("Velocidade do tiro")]
    public float speed;

    [Header("Tempo de vida")]
    public float Timer;
    public float TempoDeVida;

    Rigidbody2D rb;

    [SerializeField]
    private int damage = 1;

    [SerializeField]
    private bool haveCriticalPowerUp;

    [SerializeField]
    private int criticalChance = 10;

    [SerializeField]
    private int criticalMultiplier = 2;

    [SerializeField]
    private EventReference criticalAttack;

    [SerializeField]
    private GameObject criticalParticle;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        haveCriticalPowerUp = PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.Critical).isActivated;
        if (PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.Damage).isActivated)
        {
            damage += 1;
        }
    }


    private bool IsCriticalAttack()
    {
        if (Random.Range(0, 99) < criticalChance)
        {
            return true;
        }
        return false;
    }

    public int GetDamage(Transform position)
    {
        if (haveCriticalPowerUp)
        {
            if (IsCriticalAttack())
            {
                Instantiate(criticalParticle, position);
                RuntimeManager.PlayOneShot(criticalAttack);
                return damage * criticalMultiplier;
            }
        }
        return damage;
    }


    private void Update()
    {
        if (!GameManager.Instance.Pause)
        {
            Timer += Time.deltaTime;
            transform.Translate(Vector2.right * speed * Time.deltaTime);

            if (Timer > TempoDeVida)
            {
                Reciclar();
            }
        }
        else
        {
            rb.velocity = Vector2.zero;
        }
    }


    public void Reciclar()
    {
        gameObject.SetActive(false);
        Timer = 0;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Parede")|| collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Boss") || collision.gameObject.CompareTag("Barreira"))
        {
            Reciclar();
        }

    }
}
