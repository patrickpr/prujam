using FMODUnity;
using System.Collections;
using UnityEngine;

public class PlayerLife : MonoBehaviour
{
    [Header("Vida")]
    public int HP;
    public int MaxHP;
    public int hearthsCount;
    public bool die;
    public bool DieWait;

    [Header("Tomar dano")]
    public bool canTakeDamage;
    public float TimeToGetDmg;
    private bool deathPlayed;

    Animator Anim;

    [SerializeField] 
    private EventReference playerHitSound;
    [SerializeField] 
    private EventReference itemSound;
    [SerializeField] 
    private EventReference coinSound;
    [SerializeField]
    private EventReference deathMusic;
    [SerializeField]
    private EventReference criticalAttack;

    FMOD.Studio.Bus bus;

    [SerializeField]
    private int damage = 2;

    [SerializeField]
    private bool haveCriticalPowerUp;

    [SerializeField]
    private int criticalChance = 10;

    [SerializeField]
    private int criticalMultiplier = 2;


    [Header("TemposDosTemporarios")]
    public bool DmgUP;
    public float TimerDmg;
    int InicialDmg;
    [HideInInspector] public bool AdicionouDmg;

    public bool CriticoUP;
    public float TimerCritico;

    GerenciadorGame Gerenciador;
    public GameObject criticalParticle;

    private void Awake()
    {        
        Anim = GetComponent<Animator>();
        DieWait = false;
        bus = RuntimeManager.GetBus("Bus:/");
        GameManager.Instance.SetStartGold();
        Gerenciador = FindObjectOfType<GerenciadorGame>();
    }


    private void Start()
    {
        haveCriticalPowerUp = PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.Critical).isActivated;
        if (PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.Damage).isActivated)
        {
            damage += 1;
        }

        if (PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.HP).isActivated) 
        {
            MaxHP = 3;
        }
        else
        {
            MaxHP = 2;
        }

        HP = MaxHP;
        hearthsCount = MaxHP;
        InicialDmg = damage;
    }

    private bool IsCriticalAttack()
    {
        if (Random.Range(0, 99) < criticalChance)
        {
            return true;
        }
        return false;
    }

    public int GetDamage(Transform position)
    {
        if(haveCriticalPowerUp)
        {
            if (IsCriticalAttack())
            {
                RuntimeManager.PlayOneShot(criticalAttack);
                Instantiate(criticalParticle, position);
                return damage * criticalMultiplier;
            }
        }
        return damage;
    }

    public void TakeDamage()
    {
        RuntimeManager.PlayOneShot(playerHitSound);
        if(canTakeDamage && HP>0)
        {
                HP--;
                die = false; 
                CameraShake.instance.StartShake(0.1f, 0.1f);
                StartCoroutine(DamageFlicker());      
        }

        if (HP <= 0)
        {
            HP = 0;
            CameraShake.instance.StartShake(0.1f, 0.1f);
            Anim.SetTrigger("Damage");
            die = true;
        }
    }

    IEnumerator DamageFlicker()
    {
        canTakeDamage = false;
        Anim.SetTrigger("Damage");
        yield return new WaitForSeconds(TimeToGetDmg);
        canTakeDamage = true;
    }

    private void Update()
    {
        Anim.SetBool("Die", die);
        if (die)
        {
            if (!deathPlayed)
            {
                deathPlayed = true;
                bus.stopAllEvents(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                RuntimeManager.PlayOneShot(deathMusic);
                ItemManager.Instance.RemoveKeys();
                GameManager.Instance.SetDeathGold();
            }
            gameObject.GetComponent<PlayerControll>().Movimento = Vector2.zero;
        }

        if (CriticoUP)
        {
            TimerCritico -= Time.deltaTime;
            if (TimerCritico <= 0)
            {
                CriticoUP = false;
                haveCriticalPowerUp = false;
                TimerCritico = 0;
            }
            else
            {
                haveCriticalPowerUp = true;
            }
        }

        if (DmgUP)
        {
            TimerDmg -= Time.deltaTime;
            if (TimerDmg <= 0)
            {
                DmgUP = false;
                damage = InicialDmg;
                TimerDmg = 0;
            }
            else
            {
                if (!AdicionouDmg)
                {
                    damage += 1;
                    AdicionouDmg = true;
                }
            }
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
                                                                            //Perigos
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (!die)
            {
                TakeDamage();
            }
        }
        if (collision.gameObject.CompareTag("TiroInimigo"))
        {
            if (!die)
            {
                TakeDamage();
            }
        }
        

        //Ajuda

        if (collision.gameObject.CompareTag("Pao"))
        {
            if (HP < MaxHP)
            {
                HP += 1;
                Destroy(collision.gameObject);
                RuntimeManager.PlayOneShot(itemSound);
            }
        }                                                           

        if (collision.gameObject.CompareTag("Moeda"))
        {
            GameManager.Instance.Gold++;
            Destroy(collision.gameObject);
            RuntimeManager.PlayOneShot(coinSound);
        }       

        if (collision.gameObject.CompareTag("LocalDialogo"))
        {
            gameObject.GetComponent<FinalDoJogo>().FinalGame = true;
            gameObject.GetComponent<FinalDoJogo>().MoveToBoss = true;
            collision.gameObject.SetActive(false);
        }

        if (collision.gameObject.CompareTag("VoltaSalaBoss"))
        {
            Gerenciador.AtivaTelaLoja();
        }

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Espinhos"))
        {
            if (!die)
            {
                TakeDamage();
            }
        }
        if (collision.gameObject.CompareTag("DanoBoss"))
        {
            if (!die)
            {
                TakeDamage();
            }
        }

        if (collision.gameObject.CompareTag("Pontes"))
        {
            //Detecta o jogador na sala
            CameraController.instance.InRoom(false);
        }

        if (collision.gameObject.CompareTag("Empurrao"))
        {
            if (gameObject.GetComponent<Rigidbody2D>().velocity.y >= 0)
            {
                Vector2 novapos = transform.position;
                novapos.y += 3f * Time.deltaTime;
                transform.position = novapos;
            }
        }
    }

    public void Dead()
    {
        DieWait = true;
    }

}
