using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class FinalDoJogo : MonoBehaviour
{

    public float Velocity;
    public bool MoveToBoss;
    public bool ChegouNoLocal;

    public GameObject HUD;

    public GameObject PauseButton;

    public GameObject LocalFinal;

    public GameObject cartafinal;

    public GameObject Dialogo;

    public bool ChamouDialogo;

    public bool FinalGame;

    private void Update()
    {
        if(FinalGame)
        {
            if (MoveToBoss)
            {
                GameManager.Instance.Pause = true;
                HUD.SetActive(false); 
                PauseButton.SetActive(false);
                float posDistance = math.abs(LocalFinal.transform.position.y - transform.position.y);
                if (posDistance < 0.2)
                {
                    ChegouNoLocal = true;
                    transform.position = LocalFinal.transform.position;
                }
                else
                {
                    float PosY = transform.position.y + Velocity * Time.deltaTime;
                    Vector2 NewPos = new Vector2(transform.position.x, PosY);
                    transform.position = NewPos;

                    gameObject.GetComponent<Animator>().SetFloat("speed", 1);
                    gameObject.GetComponent<Animator>().SetFloat("Vertical", 1);
                    gameObject.GetComponent<Animator>().SetFloat("VerticalIdle", 1);
                    gameObject.GetComponent<Animator>().SetFloat("Horizontal", 0);
                    gameObject.GetComponent<Animator>().SetFloat("HorizontalIdle", 0);
                }
            }
        }

        if (ChegouNoLocal)
        {
            MoveToBoss = false;

            if (!ChamouDialogo)
            {
                PauseButton.SetActive(true);
                HUD.SetActive(true);
                Dialogo.SetActive(true);
                ChamouDialogo = true;                
            }

            // acontecer no dialogo: gameObject.GetComponent<Animator>().SetTrigger("EntregaCarta");
            //cartafinal.SetActive(true);
        }
    }

}
