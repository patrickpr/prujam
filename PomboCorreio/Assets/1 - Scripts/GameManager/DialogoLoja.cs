using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogoLoja : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI Texto;
    [SerializeField] string[] Linhas;
    [SerializeField] float VelocidadeDeDigitar;
    //[SerializeField] Image UiRosto;


    [SerializeField] private int index;

    void OnEnable()
    {
        GameManager.Instance.Pause = true;
        Texto.text = string.Empty;
        StartDialogue();
     }

    void StartDialogue()
    {
        index = 0;
        StartCoroutine(LetraPorLetra());
     }

    public void ProximoTexto()
    {
        if (Texto.text == Linhas[index])
        {
            if (index < Linhas.Length - 1)
            {
                index++;
                Texto.text = string.Empty;
                StartCoroutine(LetraPorLetra());
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
        else
        {
            StopAllCoroutines();
            Texto.text = Linhas[index];
        }

    }

    IEnumerator LetraPorLetra()
    {
        foreach (char Letra in Linhas[index].ToCharArray())
        {
            Texto.text += Letra;
            yield return new WaitForSeconds(VelocidadeDeDigitar);
        }
    }
}
