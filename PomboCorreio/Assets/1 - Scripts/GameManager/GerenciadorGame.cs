using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GerenciadorGame : MonoBehaviour
{
    PlayerLife Player;
    public GameObject DerrotaScreen;
    public GameObject VitoriaScreen;
    public GameObject PauseScreen;
    public bool StartDialog;
    FinalDoJogo FinalJogo;


    public GameObject ObjectoDeVoltarLoja;
    public GameObject TelaDeVoltarLoja;
    public TextMeshProUGUI TextoLoja;
    public GameObject FechadorDeLoja;

    public bool TaNaGarantia;

    Boss Boss;

    public GameObject DialogoFinal;

    public GameObject[] TutorialArco;

    bool callDialogueFinal;

    [SerializeField]
    private EventReference winMusic;

    FMOD.Studio.Bus bus;

    private bool winMusicPlayed;

    private void Awake()
    {
        bus = RuntimeManager.GetBus("Bus:/");
    }



    private void Start()
    {
        Player = FindObjectOfType<PlayerLife>();
        FinalJogo = FindObjectOfType<FinalDoJogo>();

        Boss = FindObjectOfType<Boss>();
        
        if (PowerUpManager.Instance.GetPowerUpById((int)PowerUpManager.IDs.Bow).isActivated)
        {
            TutorialArco[0].SetActive(true);
            TutorialArco[1].SetActive(true);
        }
        else
        {
            TutorialArco[0].SetActive(false);
            TutorialArco[1].SetActive(false);
        }

        if (GameManager.Instance.ContadorDeMortes < 1)
        {
            ObjectoDeVoltarLoja.SetActive(false);
            TextoLoja.enabled = false;
            FechadorDeLoja.SetActive(true);
        }
        else
        {
            ObjectoDeVoltarLoja.SetActive(true);
            TextoLoja.enabled = true;
            FechadorDeLoja.SetActive(false);
        }


    }

    private void Update()
    {
        

        if (Boss.die)
        {
            if(!callDialogueFinal)
            {
                GameManager.Instance.Pause = true;
                DialogoFinal.SetActive(true);
                callDialogueFinal = true;
            }
        }

        if (Player.DieWait)
        {
            GameManager.Instance.Pause = true;
            DerrotaScreen.SetActive(true);
        }
        else
        {
            DerrotaScreen.SetActive(false);
        }


        if (GameManager.Instance.Pause && !Player.DieWait && !StartDialog && !FinalJogo.MoveToBoss && !Boss.die && !FinalJogo.FinalGame &&!TaNaGarantia)
        {
            PauseScreen.SetActive(true);
        }
        else
        {
            PauseScreen.SetActive(false);
        }

    }

    public void PausaJogo()
    {
        GameManager.Instance.Pause = !GameManager.Instance.Pause;
        SoundManager.Instance.PauseEffect(GameManager.Instance.Pause);
    }


    public void AtivaTelaVitoria()
    {
        VitoriaScreen.SetActive(true); 
        if (!winMusicPlayed)
        {
            winMusicPlayed = true;
            bus.stopAllEvents(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            RuntimeManager.PlayOneShot(winMusic);
        }
    }


    public void AtivaTelaLoja()
    {
        GameManager.Instance.Pause = true;
        TaNaGarantia = true;
        TelaDeVoltarLoja.SetActive(true);
    }

    public void DesativaTelaLoja()
    {
        TelaDeVoltarLoja.SetActive(false);
        TaNaGarantia = false;
        GameManager.Instance.Pause = false;
    }
}
