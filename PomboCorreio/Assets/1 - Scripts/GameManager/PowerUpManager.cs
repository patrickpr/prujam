using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameManager;

public class PowerUpManager : MonoBehaviour
{
    public static PowerUpManager Instance;
    private List<PowerUp> PowerUps = new List<PowerUp>();

    public enum IDs
    {
        HP = 0,
        Dash = 1,
        Bow = 2,
        Reflect = 3,
        Damage = 4,
        Critical = 5,
    }

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        PowerUp UP_HP = new PowerUp((int)IDs.HP, "TESTE Ganha uma vida extra");
        PowerUp UP_Dash = new PowerUp((int)IDs.Dash, "TESTE Diminui o tempo de espera entre dashs");
        PowerUp UP_Bow = new PowerUp((int)IDs.Bow, "TESTE habilita o uso do arco e flecha");
        PowerUp UP_Reflect = new PowerUp((int)IDs.Reflect, "TESTE Reflete projeteis ao acertar com a espada");
        PowerUp UP_Damage = new PowerUp((int)IDs.Damage, "TESTE Causa o dobro de dano nos inimigos");
        PowerUp UP_Critical = new PowerUp((int)IDs.Critical, "TESTE Chance de ter critico");

        PowerUps.Add(UP_HP);
        PowerUps.Add(UP_Dash);
        PowerUps.Add(UP_Bow);
        PowerUps.Add(UP_Reflect);
        PowerUps.Add(UP_Damage);
        PowerUps.Add(UP_Critical);
    }
    
    public PowerUp GetPowerUpById(int id)
    {
        return PowerUps.Find(x => x.id == id);
    }
}
