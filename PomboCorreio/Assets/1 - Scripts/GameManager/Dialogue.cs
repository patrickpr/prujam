using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI Texto;
    [SerializeField] string[] Linhas;
    [SerializeField] float VelocidadeDeDigitar;
    [SerializeField] Image UiRosto;
    [SerializeField] Sprite[] Rostos;

    [SerializeField]private int index;

    Boss Boss;
    FinalDoJogo FinalJogo;
    GerenciadorGame GerenciadorGame;

    [SerializeField] bool UltimaCena;

    [SerializeField] private Button BotaoPauseDesativar;

    private void Awake()
    {
        FinalJogo = FindObjectOfType<FinalDoJogo>();
        Boss = FindObjectOfType<Boss>();
        GerenciadorGame = FindObjectOfType<GerenciadorGame>();
    }

    void OnEnable()
    {
        GameManager.Instance.Pause=true;
        Texto.text = string.Empty;
        StartDialogue();
        BotaoPauseDesativar.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        BotaoPauseDesativar.gameObject.SetActive(true);
    }


    void Update()
    {
        if (index == 0 || index == 2 || index == 4 || index == 5)
        {
            UiRosto.sprite = Rostos[0];
        }
        else if (index == 1 || index == 3)
        {
            UiRosto.sprite = Rostos[1];
        }
        else
        {
            UiRosto.sprite = Rostos[2];
        }
    }

    void StartDialogue()
    {
        index = 0;
        StartCoroutine(LetraPorLetra());
        SoundManager.Instance.BossMusic(0);
    }

    public void ProximoTexto()
    {
        if (Texto.text == Linhas[index])
        {
            if (index < Linhas.Length - 1)
            {
                index++;
                Texto.text = string.Empty;
                StartCoroutine(LetraPorLetra());
            }
            else if(!UltimaCena)
            {
                gameObject.SetActive(false);
                Boss.StartedFight = true;
                FinalJogo.FinalGame = false;
                GameManager.Instance.Pause = false;
                SoundManager.Instance.BossMusic(1);
            }
            else
            {
                gameObject.SetActive(false);
                GerenciadorGame.AtivaTelaVitoria();
            }
        }
        else
        {
            StopAllCoroutines();
            Texto.text = Linhas[index];
        }
       
    }

    IEnumerator LetraPorLetra()
    {
        foreach (char Letra in Linhas[index].ToCharArray())
        {
            Texto.text += Letra;
            yield return new WaitForSeconds(VelocidadeDeDigitar);
        }
    }
}
