using FMODUnity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class TrocadorDeCena : MonoBehaviour
{
    public static TrocadorDeCena Instance;

    public bool trocando;

    public GameObject Transition;
    int IndexScene;


    [SerializeField]
    private EventReference ButtonClickSound;

    [SerializeField]
    private EventReference ButtonHoverSound;

    private void Start()
    {
        trocando = false;

        Transition = Transicao.Instance.gameObject;

        if (Transition == null)
        {
            return;
        }
    }

    private void Update()
    {
        Transition.transform.position = transform.position;
    }


    public void PlayButtonClickSound()
    {
        RuntimeManager.PlayOneShot(ButtonClickSound);
    }

    public void PlayButtonHoverSound()
    {
        RuntimeManager.PlayOneShot(ButtonHoverSound);
    }
       

    public void Menu()
    {
        StartCoroutine(WaitTransition(2));
        GameManager.Instance.Pause = false;
        SoundManager.Instance.PauseEffect(false);
        ItemManager.Instance.RemoveKeys();
    }
    public void CutSceneMenu()
    {
        StartCoroutine(CutSceneToMenu(2));
        SoundManager.Instance.IntroToMenuMusicTransition();
        GameManager.Instance.Pause = false;
        SoundManager.Instance.PauseEffect(false);
    }


    public void Jogo()
    {
        StartCoroutine(WaitTransition(3));
        GameManager.Instance.Pause = false;
        SoundManager.Instance.PauseEffect(false);
    }
   

    public void Loja()
    {
        GameManager.Instance.ContadorDeMortes++;
        StartCoroutine(WaitTransition(4));
        SoundManager.Instance.PauseEffect(false);
        ItemManager.Instance.RemoveKeys();
    }

    //public void LojaComMorte()
    //{

    //    GameManager.Instance.ContadorDeMortes++; 
    //    StartCoroutine(WaitTransition(3));
    //}

    public void Options()
    {
        StartCoroutine(WaitTransition(5));
        GameManager.Instance.Pause = false;
        SoundManager.Instance.PauseEffect(false);
    }

    public void SairDoJogo()
    {
        Application.Quit();
    }

    //Fun��o para identificar o index e escolher qual musica vai tocar
    void choseMusic()
    {
        switch (IndexScene)
        {
            case 2:
                SoundManager.Instance.PlayMenuMusic();
                break;

            case 3:
                SoundManager.Instance.PlayGameMusic();
                break;

            case 4:
                SoundManager.Instance.PlayShopMusic();
                break;
        }
    }


    IEnumerator WaitTransition(int index)
    {
        if (Transition == null)
        {
            SceneManager.LoadScene(index);
        }
        else
        {
            if (!trocando)
            {
                trocando = true;
                Transition.SetActive(true);
                Transition.GetComponent<Animator>().SetTrigger("IndoParaCena");
                yield return new WaitForSeconds(.8f);
                SceneManager.LoadScene(index);
                IndexScene = index;
                choseMusic();
                Transition.GetComponent<Animator>().SetTrigger("ChegandoNaCena");
                yield return new WaitForSeconds(1);
                Transition.SetActive(false);
                trocando = false;
                SoundManager.Instance.PauseEffect(false);
            }
        }
    }

    IEnumerator CutSceneToMenu(int index)
    {
        if (Transition == null)
        {
            SceneManager.LoadScene(index);
        }
        else
        {
            if (!trocando)
            {
                trocando = true;
                Transition.SetActive(true);
                Transition.GetComponent<Animator>().SetTrigger("IndoParaCena");
                yield return new WaitForSeconds(.8f);
                SceneManager.LoadScene(index);
                Transition.GetComponent<Animator>().SetTrigger("ChegandoNaCena");
                yield return new WaitForSeconds(1);
                Transition.SetActive(false);
                trocando = false;
                SoundManager.Instance.PauseEffect(false);
            }
        }
    }

}
