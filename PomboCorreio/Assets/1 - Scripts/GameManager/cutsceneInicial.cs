using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using FMODUnity;

public class cutsceneInicial : MonoBehaviour
{
    public int QualCena;

    public TextMeshProUGUI Texto;

    public string[] Textos;

    Animator Anim;

    public RectTransform posicao;

    Vector2 newpos;


    [SerializeField] private EventReference papinhoPombos1;
    [SerializeField] private EventReference papinhoPombos2;
    //Nao tem 3 pq o terceiro quadrinho n�o tem texto
    [SerializeField] private EventReference papinhoPombos4;
    [SerializeField] private EventReference papinhoPombos5;

    private void Start()
    {
        Anim = GetComponent<Animator>();
        newpos = posicao.localPosition;
    }

    public void Fim1()
    {
        if(QualCena !=5)
        {
            Anim.SetTrigger("trocaCena");
        }
        else
        {
            gameObject.GetComponent<TrocadorDeCena>().CutSceneMenu();
        }       
    }

    public void Cena(int NumeroCena)
    {
        QualCena = NumeroCena;
    }


    private void Update()
    { 
        switch (QualCena) 
        {
            case 1:
                Texto.text = Textos[0];
                break;
            case 2:
                Texto.text = Textos[1];
                newpos.x = 97;
                posicao.localPosition = newpos;
                break;
            case 3:
                break;
            case 4:
                Texto.text = Textos[2];
                newpos.y = 381;
                newpos.x = -111;
                posicao.localPosition = newpos;
                break;
                case 5:
                Texto.text = Textos[3];
                newpos.y = 362.2f;
                newpos.x = -171.3f;
                posicao.localPosition = newpos;
                break;
        } 
    }

    public void TocaConversa1()
    {
        RuntimeManager.PlayOneShot(papinhoPombos1);
    }
    public void TocaConversa2()
    {
        RuntimeManager.PlayOneShot(papinhoPombos2);
    }

    //Nao tem 3 pq o terceiro quadrinho n�o tem texto

    public void TocaConversa4()
    {
        RuntimeManager.PlayOneShot(papinhoPombos4);
    }
    public void TocaConversa5()
    {
        RuntimeManager.PlayOneShot(papinhoPombos5);
    }

}
