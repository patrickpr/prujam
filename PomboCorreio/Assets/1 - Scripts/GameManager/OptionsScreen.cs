using FMOD;
using FMODUnity;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OptionsScreen : MonoBehaviour
{
    public Slider SliderMusic;
    public Slider SliderSoundEffects;

    public Toggle FullScreenButton;

    public TMP_Dropdown ResolutionDropdown;
    string resolution;
    int width, height;
    string[] resolutionParts;

    private List<string> resolutionList = new List<string>()
    {
        "1920x1080",
        "1600x900",
        "1366x768",
        "1280x720",
        "854x480"
    };

    private void Start()
    {
        if (PlayerPrefs.HasKey("FullScreen"))
        {
            FullScreenButton.isOn = PlayerPrefs.GetInt("FullScreen") == 1;       //Recebe o valor salvo do playerprefs nessa variavel
        }
        else
        {
            FullScreenButton.isOn = Screen.fullScreen;
        }

        SliderMusic.value = SoundManager.Instance.MusicVolume;
        SliderSoundEffects.value = SoundManager.Instance.SfxVolume;
        
        ResolutionDropdown.ClearOptions();
        ResolutionDropdown.AddOptions(resolutionList);

        int savedResolutionIndex = PlayerPrefs.GetInt("ResolutionIndex");
        if (savedResolutionIndex >= 0 && savedResolutionIndex < resolutionList.Count)
        {
            ResolutionDropdown.value = savedResolutionIndex;
            OnResolutionChange(savedResolutionIndex);
        }
        else
        {
            ResolutionDropdown.value = 0;
            OnResolutionChange(0);
        }
    }

    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
        if (isFullScreen)
        {
            PlayerPrefs.SetInt("FullScreen", 1);                                //Guarda valor
        }
        else
        {
            PlayerPrefs.SetInt("FullScreen", 0);                                //Guarda valor
        }
    }

    public void OnResolutionChange(int index)
    {
        string resolution = resolutionList[index];
        string[] resolutionParts = resolution.Split('x');
        int width = int.Parse(resolutionParts[0]);
        int height = int.Parse(resolutionParts[1]);

        ResolutionDropdown.value = index;
        Screen.SetResolution(width, height, Screen.fullScreen);

        PlayerPrefs.SetInt("ResolutionIndex", index);                           //Guarda valor
    }

    public void ChangeSFXVolume(float value)
    {
        RuntimeManager.StudioSystem.setParameterByName("SFXVolume", value);
        SoundManager.Instance.SfxVolume = value;
        PlayerPrefs.SetFloat("SFXVolume", value);                               //Guarda valor
    }

    public void ChangeMusicVolume(float value)
    {
        RuntimeManager.StudioSystem.setParameterByName("MusicVolume", value);
        SoundManager.Instance.MusicVolume = value;
        PlayerPrefs.SetFloat("MusicVolume", value);                             //Guarda valor
    }

    public void SalvarPlayerPrefs()
    {
        PlayerPrefs.Save();
    }


}
