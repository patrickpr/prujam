using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{
    PlayerLife LifePlayer;
    public Image[] Hearths;
    public Sprite FillHearth, noHearth;

    public TextMeshProUGUI NumeroCartas;
    public TextMeshProUGUI MoedasText;

    void Start()
    {
        LifePlayer = FindObjectOfType<PlayerLife>();
        UpdateLetters();
    }

    // Update is called once per frame
    void Update()
    {
        QuantidadeCoracoes();
        MoedasText.text = GameManager.Instance.Gold.ToString();
    }

    public void UpdateLetters()
    {
        NumeroCartas.text = ItemManager.Instance.GetNumberOfLetters().ToString() + "/5";
    }

    void QuantidadeCoracoes()
    {
        //Faz uma contagem do I at� o tamanho dos cora��es maximos(posso colocar 10 cora��es no array), se eu quiser que apare��o apenas 3, eu coloco no hearts count que est� no jogador o valor de tr�s e vai ser validado no if abaixo
        for (int i = 0; i < Hearths.Length; i++)
        {
            if (i < LifePlayer.HP)
            {
                Hearths[i].sprite = FillHearth;
            }
            else
            {
                Hearths[i].sprite = noHearth;
            }

            if (i < LifePlayer.hearthsCount)
            {
                Hearths[i].enabled = true;
            }
            else
            {
                Hearths[i].enabled = false;
            }
        }

    }
}
