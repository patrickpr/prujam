using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public int ContadorDeMortes;

    public int Gold;
    public int StartGold;
    public int CartasColetadas;

    public GameObject[] PossiveisObjetos;
    public float raio;
    public bool Pause;

    [SerializeField]
    private float porcentagemDeOuroPerdido;

    public bool JaLimpouSala;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void SetStartGold()
    {
        StartGold = Gold;
    }

    public void SetDeathGold()
    {
        var unsafeGold = Gold - StartGold;
        Gold = StartGold;
        unsafeGold -= (int)(unsafeGold * porcentagemDeOuroPerdido);
        Gold += unsafeGold;
    }


    public void DropGold(int quantidade,Vector2 Centro)
    {
        int valor = Random.Range(0, 100);

        if (valor >= 0 && valor < 5)
        {
            Instantiate(PossiveisObjetos[0], Centro, Quaternion.identity);
        }
        else if (valor <= 50 && valor >= 5)
        {
            for (int i = 0; i < quantidade; i++)
            {
                float angulo = Random.Range(0, 2 * math.PI);
                Vector2 posicao = new Vector2(Centro.x + raio * Mathf.Cos(angulo), Centro.y + raio * Mathf.Sin(angulo));

                Instantiate(PossiveisObjetos[1], posicao, Quaternion.identity);
            }
        }
        else
        {
            return;
        }
    }
}
