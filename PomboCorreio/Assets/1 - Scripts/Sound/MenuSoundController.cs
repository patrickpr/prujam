using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MenuSoundController : MonoBehaviour
{
    FMOD.Studio.Bus bus;

    private void Start()
    {
        bus = RuntimeManager.GetBus("Bus:/");
    }

    public void stopMusic()
    {
        bus.stopAllEvents(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
}
