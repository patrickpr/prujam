using FMOD.Studio;
using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;  

    FMOD.Studio.Bus bus;

    private EventInstance EventInstance;

    public float MusicVolume;

    public float SfxVolume;

    private void Awake()
    {
        bus = RuntimeManager.GetBus("Bus:/");

        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        MusicVolume = 0.3f;
        SfxVolume = 0.5f;
        SetInitialParameters();
        PlayMenuMusic();
    }


    public void PauseGameParameter()
    {
        if(!GameManager.Instance.Pause)
        {
            RuntimeManager.StudioSystem.setParameterByName("Pause", 0);
        }
        else
        {
            RuntimeManager.StudioSystem.setParameterByName("Pause", 1);
        }
    }

    public void IntroToMenuMusicTransition()
    {
        EventInstance.setParameterByName("EnterMenu", 1);
        //RuntimeManager.StudioSystem.setParameterByName("EnterMenu", 1);
    }

    public void PlayMenuMusic()
    {
        bus.stopAllEvents(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        EventInstance = RuntimeManager.CreateInstance("event:/Music/Menu");
        EventInstance.start();
    }

    public void PlayGameMusic()
    {
        bus.stopAllEvents(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        EventInstance = RuntimeManager.CreateInstance("event:/Music/Dungeon");
        EventInstance.start();
    }

    public void PlayShopMusic()
    {
        bus.stopAllEvents(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        EventInstance = RuntimeManager.CreateInstance("event:/Music/Shop");
        EventInstance.start();
    }

    public void BossMusic(int phase)
    {
        if(phase == 0)
        {
            bus.stopAllEvents(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            EventInstance = RuntimeManager.CreateInstance("event:/Music/Lich");
            EventInstance.start();
        }
        if(phase == 1){
            EventInstance.setParameterByName("LichBattle", 1);
        }
        if (phase == 2)
        {
            EventInstance.setParameterByName("LichPowerUp", 1);
        }
    }

    public void PauseEffect(bool value)
    {
        if (value)
        {
            RuntimeManager.StudioSystem.setParameterByName("Pause", 1);
        }
        else
        {
            RuntimeManager.StudioSystem.setParameterByName("Pause", 0);
        }
    }

    public void SetInitialParameters()
    {
        if (PlayerPrefs.HasKey("MusicVolume"))
        {
            MusicVolume = PlayerPrefs.GetFloat("MusicVolume");
            RuntimeManager.StudioSystem.setParameterByName("MusicVolume", MusicVolume);            //Recebe o valor salvo do playerprefs nessa variavel
        }
        else
        {
            RuntimeManager.StudioSystem.setParameterByName("MusicVolume", 0.5f);
        }
        if (PlayerPrefs.HasKey("SFXVolume"))
        {
            SfxVolume = PlayerPrefs.GetFloat("SFXVolume");
            RuntimeManager.StudioSystem.setParameterByName("SFXVolume", SfxVolume);        //Recebe o valor salvo do playerprefs nessa variavel
        }
        else
        {
            RuntimeManager.StudioSystem.setParameterByName("SFXVolume", 0.5f);
        }       
    }
}
