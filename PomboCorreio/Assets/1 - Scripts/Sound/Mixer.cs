using FMOD.Studio;
using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mixer : MonoBehaviour
{
    public void ChangeSFXVolume(float value)
    {
        RuntimeManager.StudioSystem.setParameterByName("SFXVolume", value);
    }

    public void ChangeMusicVolume(float value)
    {
        RuntimeManager.StudioSystem.setParameterByName("MusicVolume", value);
    }
}
