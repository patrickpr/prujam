using System.Collections;
using FMODUnity;
using System.Collections.Generic;
using UnityEngine;

public class Portas : MonoBehaviour
{
    Animator Anim;

    [SerializeField] EventReference PortaSound;

    private void Start()
    {
        Anim = GetComponent<Animator>();
    }

    public void Som()
    {
        RuntimeManager.PlayOneShot(PortaSound);
    }

}
