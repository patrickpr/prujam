using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashHUD : MonoBehaviour
{
    PlayerControll Dash;

    public Slider SliderDash;

    private void Start()
    {
        Dash = FindObjectOfType<PlayerControll>();
        SliderDash.maxValue = Dash.CdDash;
    }

    private void Update()
    {
        SliderDash.value = Dash.TimerCdDash;
    }
}
