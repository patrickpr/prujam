using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class Potes : MonoBehaviour
{
    Animator Anim;
    [SerializeField] int size;
    int quantidade;


    [SerializeField] private EventReference SomQuebraPote;

    private void Start()
    {
        Anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Attack"))
        {
            Anim.SetTrigger("Hit");
            RuntimeManager.PlayOneShot(SomQuebraPote);
        }
    }

    public void Quebrou()
    {
        switch (size)
        {
            case 1:
                quantidade = Random.Range(5, 10);
                break;
            case 2:
                quantidade = Random.Range(8, 12);
                break;
            case 3:
                quantidade = Random.Range(10, 15);
                break;
        }
        GameManager.Instance.DropGold(quantidade, transform.position);
    }

    public void quebrouPote()
    {
        RuntimeManager.PlayOneShot(SomQuebraPote);
    }
}
