using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PortaDireitaInicial : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI TextoDessaPorta;
    
    [HideInInspector]public Animator Anim;

    //public bool HasEnemy;
    public bool abriu;

    [SerializeField] EventReference PortaSound;

    private void Start()
    {
        Anim = GetComponent<Animator>();
        TextoDessaPorta.enabled = false; 
        Anim = GetComponent<Animator>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (!abriu)
            {
                TextoDessaPorta.enabled = true;               
            }
            else
            {
                TextoDessaPorta.enabled = false;
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            TextoDessaPorta.enabled = false;
        }
    }
    
    public void Som()
    {
        RuntimeManager.PlayOneShot(PortaSound);
    }

    public void AbriuAporta()
    {
        abriu = true;
    }
}
